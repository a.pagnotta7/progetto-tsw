let i  = 0;

function changeValue(){
    if(i == 0) i = 1;
    else i = 0;
}

buttonUpdate();

function buttonUpdate() {
    changeValue();
    let divCard = document.getElementById("divCard");
    let formCard = document.getElementById("formCard");

    if(i == 0){
        divCard.style.display = "none";
        formCard.style.display = "block";
        changeValue();
    }else{
        divCard.style.display = "block";
        formCard.style.display = "none";
        changeValue();
    }
}


function setAction(form) {
    onEnter();
    return false;
}

function onEnter(){
    $.ajax({
        type: "POST",
        url: "UserUpdate?newName=" + document.getElementById("newName").value + "&newCognome=" + document.getElementById("newSurname").value + "&newEmail=" + document.getElementById("newEmail").value + "&newPassword=" + document.getElementById("newPassword").value,
        success: function(data)
        {
            redirect("userPage.jsp");
        }
    });
}

function onEnter1(textField, type, area){
    $.post(
        "UserUpdate",
        {
            type: type,
            value: document.getElementById(textField).value
        }
    ).done(function(data){
        if(type === 4){
            document.getElementById(area).innerHTML = "********";
        }
        else{
            document.getElementById(area).innerHTML = document.getElementById(textField).value;
        }
    })
}
