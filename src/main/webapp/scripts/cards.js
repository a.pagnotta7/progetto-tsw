let id = "";
let cardType = -1;
function setElementID(newId){
    id=newId;
}
function setCardType(newType){
    cardType=newType;
}
function createCards(result){
    if(cardType === 0)
    {
        createProductCards(result);
    }
}


function createProductCards(result) {
    let products = result;
    document.getElementById(id).innerHTML = "";
    let cardHTML = "";

    for(let i = 0; i < products.length; i++) {
        cardHTML += "<div class=\"card\">"
        cardHTML += "<a href=\"ProductPage?id="+ products[i]["id"] + "\">" + "<img src=\"" + products[i]["immagine"] + "\" alt=\"" + products[i]["nome"] + "\">" + "</a>";

        cardHTML += "<h1 class=\"productName\">" + products[i]["nome"] + "</h1>";

        cardHTML += "<span class=\"price\">" + (products[i]["costo"] - products[i]["costo"] * products[i]["sconto"] / 100) + "&euro;</span>";
        if(products[i]["sconto"] > 0) {
            cardHTML += "<span class=\"realPrice\">" + products[i]["costo"] + "&euro;</span>" + "<span class=\"DiscountTag\">" + products[i]["sconto"] + "%</span>";
        }
        cardHTML += "<button id =\"button" + products[i]["id"] + "\" onclick=\"addToCart(" + products[i]["id"] + ", 1); changeLabel(" + products[i]["id"] + "," + false +")\"> Aggiungi al carrello </button> </div>";
    }

    cardHTML += "<div id=\"padding1\" class=\"card padding\"></div>";
    cardHTML += "<div id=\"padding2\" class=\"card padding\"></div>";

    document.getElementById(id).innerHTML = cardHTML;
}
