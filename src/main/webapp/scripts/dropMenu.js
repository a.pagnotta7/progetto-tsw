let open = 0;
function dropMenu() {
    let width = screen.width;

    if(width <= 925){
        return;
    }
    if(open == 0){
        open = 1;
        document.getElementById('drop-menu').style.visibility = "visible";
        document.getElementById('menu-cart2').style.display = "block";
        document.getElementById('menu-products2').style.display = "block";
        document.getElementById('menu-aboutUs2').style.display = "block";
        document.getElementById('menu-contactUs2').style.display = "block";
        document.getElementById('menu-log2').style.display = "block";
    }else{
        open = 0;
        document.getElementById('drop-menu').style.visibility = "hidden";
        document.getElementById('menu-cart2').style.display = "none";
        document.getElementById('menu-products2').style.display = "none";
        document.getElementById('menu-aboutUs2').style.display = "none";
        document.getElementById('menu-contactUs2').style.display = "none";
        document.getElementById('menu-log2').style.display = "none";
    }
}

window.addEventListener('resize', function(event) {
    let width = screen.width;
    if(width > 915){
        document.getElementById('drop-menu').style.visibility = "hidden";
        document.getElementById('menu-cart2').style.display = "none";
        document.getElementById('menu-products2').style.display = "none";
        document.getElementById('menu-aboutUs2').style.display = "none";
        document.getElementById('menu-contactUs2').style.display = "none";
        document.getElementById('menu-log2').style.display = "none";
    }
}, true);