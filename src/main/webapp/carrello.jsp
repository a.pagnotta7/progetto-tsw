        <%--
  Created by IntelliJ IDEA.
  User: antonio
  Date: 06/07/23
  Time: 20:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>
<%@ page import="com.example.apicoltura_mieldoro.model.carrello.CartEntry" %>
<%@ page import="java.util.Locale" %>
<%@ page import="com.example.apicoltura_mieldoro.model.utente.UtenteBean" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Apicoltura Mieldoro - Carrello</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="stylesheet/header.css?<?php echo time(); ?>">
    <link rel="stylesheet" href="stylesheet/footer.css?<?php echo time(); ?>">
    <link rel="stylesheet" href="stylesheet/cart.css?<?php echo time(); ?>">
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Pompiere:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="icon" type="image/x-icon" href="images/favicon-32x32.png">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script src="scripts/cart.js"></script>
    <title>Apicoltura Mieldoro - Carrello</title>
</head>
<body>
<%@include file="WEB-INF/includes/header.jsp"%>
<%
    int id = 0;
    double totalPrice = 0;
    /*
    if(session.getAttribute("isLoggedIn") == null || session.getAttribute("utente") == null){
        String site = new String("index.jsp");
        response.setStatus(response.SC_MOVED_TEMPORARILY);
        response.setHeader("Location", site);
    }else{
    */
    List<CartEntry> cartL;
    if (isLoggedIn) {

        UtenteBean user = (UtenteBean) session.getAttribute("utente");
     /*
        if(user == null){
            String site = new String("index.jsp");
            response.setStatus(response.SC_MOVED_TEMPORARILY);
            response.setHeader("Location", site);
        }
     */
        id = user.getId();
        cartL = (List<CartEntry>) session.getAttribute("carrello");
        if(cartL != null) {
            for (CartEntry entry : cartL) {
                totalPrice += (entry.getProdotto().getCosto() * 100 - (entry.getProdotto().getCosto() * entry.getProdotto().getSconto())) * entry.getEntry().getQuantita();
            }
        }
    }
    /*
    if (totalPrice == 0) {
        String site = new String("index.jsp");
        response.setStatus(response.SC_MOVED_TEMPORARILY);
        response.setHeader("Location", site);
    }*/
%>
<div class="shoppingCart" id="shoppingCartDiv">
    <div class="title" id="title"> Carrello </div>
    <div id="items">
        <%
            cartL = (List<CartEntry>) session.getAttribute("carrello");
            double cartPrice = 0;
            if (cartL == null || cartL.isEmpty()) {
        %>
        <script>
            function carrelloVuoto() {
                let title = document.getElementsByClassName("title");
                title[0].innerHTML = "Carrello vuoto";
            }

            isCarrelloVuoto = 1;

            carrelloVuoto();

        </script>
        <%
        } else {
            for (CartEntry entry : cartL) {
        %>
        <script>
            isCarrelloVuoto = 0;
        </script>
        <div class="item" id="item<%=entry.getEntry().getProdotto()%>">
            <div class="imageContainer">
                <a href="ProductPage?id=<%=entry.getProdotto().getId()%>"><img src="
                        <%=entry.getProdotto().getImmagine()%>" alt="<%=entry.getProdotto().getNome()%>" class="image"></a>
            </div>
            <div>
                <input type="hidden" id="maxQuantity<%=entry.getProdotto().getId()%>" value="<%=entry.getProdotto().getQuantita()%>">
            <h2 style="display: block"><%=entry.getProdotto().getNome()%></h2>

            <div class="quantity" style="display: block">
                <button class="plusButton" onclick="addToCart(<%=entry.getProdotto().getId()%> , 0)">+</button>
                <span id="quantityOf<%=entry.getProdotto().getId()%>"><%=entry.getEntry().getQuantita()%></span>
                <button class="minusButton" onclick="subFromCart(<%=entry.getProdotto().getId()%>)">-</button>
                <button class="deleteButton" onclick="remFromCart(<%=entry.getProdotto().getId()%>)">x</button>
            </div>
            <%
                double prezzo = entry.getProdotto().getCosto() - (entry.getProdotto().getCosto() * entry.getProdotto().getSconto() / 100);
                cartPrice += entry.getEntry().getQuantita() * prezzo;
            %>
            <span style="display:none" id="singlePriceOf<%=entry.getProdotto().getId()%>"><%=prezzo%></span></div>
            <div class="totalPrice" id="totalPriceOf<%=entry.getProdotto().getId()%>">
                <%=String.format("%.2f", prezzo * entry.getEntry().getQuantita())%> &euro; </div>

        </div>
        <%
                }
            }
        %>
    </div>
    <div class="placeOrderContainer">
        <span class="cartPrice"> Totale: <span id="cartPrice"><%=String.format("%.2f", cartPrice)%></span>&euro;</span>
        <%
            if (isLoggedIn == false) {
        %>
                <button class="placeOrderButton"  onclick="redirect1()">Effettua il login</button>
            <%
            } else {
            %>
                <button class="placeOrderButton" onclick="makeOrder()">Procedi all'ordine</button>
            <%
                }
            %>

    </div>

</div>
<div class="orderContainer" id="orderDiv" style="display: none">
    <form action="OrderServlet" class="formContainer" method="post" onsubmit="return validate()">
        <div class="rowContainer">
            <h2> Indirizzo di Spedizione</h2>
            <input type="hidden" name="userID" value="<%=id%>">
            <label for="nome"><span> Nome</span></label>
            <input type="text" id="nome" name="nome" placeholder="Nome Cognome" required><br>
            <label for="email"><span> Email</span></label>
            <input type="text" id="email" name="email" placeholder="example@email.com" required><br>
            <label for="indirizzo"><span> Indirizzo</span></label>
            <input type="text" id="indirizzo" name="indirizzo" placeholder="Via Vittorio Emanuele" required><br>
            <label for="citta"> <span> Citt&agrave;</span> </label>
            <input type="text" id="citta" name="citta" placeholder="Roma" required><br>
            <label for="CAP"><span>CAP</span></label>
            <input type="text" name="CAP" id="CAP" placeholder="00100" required><br>
        </div>
        <div class="rowContainer">
            <h2>Pagamento</h2>
            <label for="cname"><span> Intestatario</span></label>
            <input type="text" id="cname" name="nomeCarta" placeholder="Nome Cognome" required><br>
            <label for="ccnum"><span> Numero carta</span></label>
            <input type="text" id="ccnum" name="numeroCarta" placeholder="0000-0000-0000-0000" required><br>
            <label for="expmonth"><span> Scadenza</span></label>
            <input type="month" id="expmonth" name="meseScadenza" placeholder="Agosto" required><br>
            <label for="cvv"><span> CVV</span></label>
            <input type="text" id="cvv" name="cvv" placeholder="333" required><br>
            <div class="deliveryContainer">
                <label for="delivery"><span> Spedizione Rapida</span></label>
                <input type="checkbox" name="spedizioneRapida" id="delivery" value="true" onchange="setFastDelPrice()">

            </div>
            <span>Totale: <span id="totalPrice"><%=String.format(Locale.US, "%.2f", totalPrice / 100)%></span>&euro;</span>
        </div>

        <input type="submit" value="Ordina" class="orderButton"> <br>
        <span id="errorMsg" style="visibility: hidden; text-align: center"></span>
    </form>
</div><br><br><br>
<script>
    function redirect() {
        window.location.replace("index.jsp");
    }

    function setFastDelPrice() {
        const baseTotal = <%=Math.ceil(totalPrice)%> / 100;
        let totalPrice = parseFloat($("#totalPrice").text());
        let fastDelivery = document.getElementById("delivery").checked;

        if(fastDelivery && totalPrice === baseTotal) {
            totalPrice += 5;
        }
        else if(!fastDelivery && totalPrice > baseTotal) {
            totalPrice -= 5;
        }

        $("#totalPrice").text(totalPrice.toFixed(2));
    }


    function redirect1() {
        window.location.href = "login.jsp";
    }

    function makeOrder() {
        if(isCarrelloVuoto != 1) {
            let order = document.getElementById("orderDiv");
            let shoppingCart = document.getElementById("shoppingCartDiv");
            order.style.display = "block";
        }else{
            alert("Il carello è vuoto, inserisci prima qualche prodotto.");
            window.location.replace("products.jsp");
        }
    }



</script>

<%@include file="WEB-INF/includes/footer.html"%>

</body>
</html>

