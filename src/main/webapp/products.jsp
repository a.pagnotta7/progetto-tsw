<%--
  Created by IntelliJ IDEA.
  User: antonio
  Date: 15/07/23
  Time: 00:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="stylesheet/header.css?<?php echo time(); ?>">
      <link rel="stylesheet" href="stylesheet/products.css?<?php echo time(); ?>">
      <link rel="stylesheet" href="stylesheet/footer.css?<?php echo time(); ?>">
      <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="icon" type="image/x-icon" href="images/favicon-32x32.png">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
      <script src="scripts/cards.js"></script>
      <script src="scripts/gridResolver.js"></script>
      <script src="scripts/cart.js"></script>
      <script src="scripts/respSearch.js"></script>
      <script src="scripts/searchBar.js"></script>
      <title>Apicoltura mieldoro - Prodotti</title>
  </head>
  <body style="margin: 0">
  <%  int type = 0;
      int indexType = 0;
      if(request.getParameter("index") != null)
          indexType = Integer.parseInt(request.getParameter("index"));
      if(indexType != 0)
          type = indexType;
  %>
    <%@include file="WEB-INF/includes/header.jsp"%>
    <div class="search-box">
        <form class="center-search" onsubmit="return setAction(this)">
            <input name="search" type="text" class="search" id="search-bar" value="" placeholder="Cerca">
                <div class="edit-filter-modal ">
                    <select id="tipo" name="type">
                        <%
                            if(indexType == 0){
                        %>
                        <option value="0" disabled selected>Tipo</option>
                        <option value="1" >Miele</option>
                        <option value="2" >Attrezzature</option>
                        <option value="3" >Accessori</option>
                        <%
                            }else if(indexType == 1){
                        %>
                        <option value="0" disabled>Tipo</option>
                        <option value="1" selected>Miele</option>
                        <option value="2" >Attrezzature</option>
                        <option value="3" >Accessori</option>
                        <%
                            }else if(indexType == 2){
                        %>
                        <option value="0" disabled>Tipo</option>
                        <option value="1" >Miele</option>
                        <option value="2" selected>Attrezzature</option>
                        <option value="3" >Accessori</option>
                        <%
                            }
                        %>
                    </select>
                    <select id="sconto" name="discount">
                        <option value="0" disabled selected>Sconto</option>
                        <option value="1">5 - 15 %</option>
                        <option value="2">15 - 30 %</option>
                        <option value="3">30 - 45 %</option>
                        <option value="4">45 - 60 %</option>
                    </select>
                    <select id="prezzo" name="price">
                        <option value="0" disabled selected>Prezzo</option>
                        <option value="1">5 - 20 €</option>
                        <option value="2">20 - 30 €</option>
                        <option value="3">30 - 40 €</option>
                        <option value="4"> >  40 €</option>
                    </select>
                    <button class="apply-button" onclick="searchProducts()">Apply</button><button class="text-button" onclick="resetFormParams()">Cancel</button>
                </div>
        </form>
    </div>
    </div><br><br><br>

    <section>
        <div class="cardContainer" id="prodotti"></div>
    </section><br><br><br><br>
    <script>
      function productsInit(){
        setCardType(0);
        setElementID("prodotti");
        $.ajax({
          url:"ProGetter?type=<%= type %>&top=0",
          success: function(result){
            createCards(result);
          }
        })
      }
      productsInit();

      function resetFormParams(){
          document.getElementById("tipo").value = "0";
          document.getElementById("sconto").value = "0";
          document.getElementById("prezzo").value = "0";
      }

      function setAction(form) {
          searchProducts();
          return false;
      }

        function searchProducts(){
            let u = "ProGetter?type=" + document.getElementById("tipo").value + "&top=0&searchString=" + document.getElementById("search-bar").value +"&discount=" + document.getElementById("sconto").value + "&price=" + document.getElementById("prezzo").value;
            $.ajax({
                url: u,
                success: function(result){
                    createCards(result);
                }
            })
        }

    </script>
    <%@include file="WEB-INF/includes/footer.html"%>
  </body>
</html>
