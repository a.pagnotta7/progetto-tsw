<%--
  Created by IntelliJ IDEA.
  User: antonio
  Date: 14/07/23
  Time: 16:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="stylesheet/header.css?<?php echo time(); ?>">
    <link rel="stylesheet" href="stylesheet/userPage.css?<?php echo time(); ?>">
    <link rel="stylesheet" href="stylesheet/footer.css?<?php echo time(); ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
    <link rel="icon" type="image/x-icon" href="images/favicon-32x32.png">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script src="scripts/cards.js"></script>
    <script src="scripts/userUpdater.js"></script>
    <script src="scripts/checkOutValidate.js"></script>
    <script src="scripts/reviewsUserPage.js"></script>

    <title>Apicoltura Mieldoro - Utente</title>
  </head>
  <body style="width: 100%; height: 100%;">

  <%@ page import="com.example.apicoltura_mieldoro.model.utente.*" %>
  <%@ page import="com.example.apicoltura_mieldoro.model.elementoOrdine.ElementoOrdineBean" %>
  <%@ page import="com.example.apicoltura_mieldoro.model.ordine.OrdineBean" %>
  <%@ page import="java.util.List" %>
  <%@ page import="com.example.apicoltura_mieldoro.model.prodotto.ProdottoBean" %>
  <%@ page import="java.util.Locale" %>
  <%
    UtenteBean utente = null;
    int id = -1;
    String nome = "";
    String cognome = "";
    String email = "";
    Boolean admin = false;
    String typeOfUser = "";
    String password = "";

    if(session.getAttribute("isLoggedIn") != null && session.getAttribute("utente") != null){
      boolean isLoggedIn = Boolean.parseBoolean((String) session.getAttribute("isLoggedIn"));
      if(isLoggedIn == false){
    %>
      <script>
      redirect();
      </script>
  <%
      }else{
        utente = (UtenteBean) session.getAttribute("utente");
        id = utente.getId();
        nome = utente.getNome();
        cognome = utente.getCognome();
        email = utente.getEmail();
        admin = utente.isAdmin();
        password = utente.getPassword();
        typeOfUser = "";
        if(admin == false) typeOfUser = "Cliente";
        else { typeOfUser = "Admin";}
      }
  }else{
    %>
      <script>
        redirect();
      </script>
    <%
    }
  %>

  <%@include file="WEB-INF/includes/header.jsp"%><br>

    <div id="divCard" class="card">
      <img src="images/profilePic.png" alt="proPic" style="width:100%">
      <h1 id="nameLabel"><%=nome%></h1>
      <h1 id="cognomeLabel"><%=cognome%></h1>
      <p class="title" id="emailLabel"><%=email%></p>
      <p id="touLabel"><%=typeOfUser%></p>
      <a href="LogoutServlet"><img src="images/right-from-bracket-solid.png"></a>
      <p><button class="cardButton" id="updateButton1" onclick="buttonUpdate()">Modifica Profilo</button></p>
    </div>

    <form id="formCard"  class="card" onsubmit="setAction(this)" style="display: none">
      <img src="images/profilePic.png" alt="proPic" style="width:100%">
      <input name="newName" id="newName" class="input" type="text" value="<%=nome%>" placeholder="Nome"/>
      <input name="newSurname" id="newSurname" class="input" type="text" value="<%=cognome%>"  placeholder="Cognome"/>
      <input name="newEmail" id="newEmail" class="input" type="email" value="<%=email%>"  placeholder="Email"/>
      <input name="newPassword" id="newPassword" class="input" type="password" value=""  placeholder="Password"/>
      <p><button class="cardButton" id="updateButton2">Modifica Profilo</button></p>

    </form>

  <% if(admin)
  {
  %>
  <h1 class="userStuff">Prodotti</h1><br>
  <table id="productsTable">
  </table>
  <div id="editProducts" class="formAdder"></div>
  <%
    } %>


  <%
    boolean noOrders = (boolean) request.getAttribute("noOrders");
    if(noOrders) {
  %>
  <h1 class="userStuff" style="display: none">Nessun ordine</h1>
  <%
  } else {
    List<OrdineBean> orders = (List<OrdineBean>) request.getAttribute("orders");
    List<ElementoOrdineBean> entries = (List<ElementoOrdineBean>) request.getAttribute("entries");
    List<ProdottoBean> products = (List<ProdottoBean>) request.getAttribute("products");
  %>
  <h1 class="userStuff">Ordini</h1>
  <table id="orderTable">
    <%

      for(OrdineBean order: orders) {
    %>
    <tr class="headerContainer<%=order.getId()%>">
      <th>Ordine numero <%=order.getId()%></th>
      <th>Data: <%=order.getDataAcquisto().toLocalDate()%></th>
      <td id="date<%=order.getId()%>" hidden="hidden"><%= order.getDataAcquisto()%></td>
      <th>Totale: <%=String.format(java.util.Locale.US, "%.2f", order.getPrezzoTotale())%></th>
      <th>Agg. recensione</th>
    </tr>
    <%
      for(ElementoOrdineBean entry: entries) {
        if(entry.getOrdine() == order.getId()) {
          for(ProdottoBean product: products) {
            if(product.getId() == entry.getProdotto()) {

              System.out.println(order.getDataAcquisto().toString());
              String date = order.getDataAcquisto().toString().replace("-","/");
              System.out.println("-" + date + "-");
    %>
    <tr>
      <td><%=product.getNome()%></td>
      <td>Quantit&agrave;: <%=entry.getQuantitaAcquistata()%></td>
      <td>Prezzo: <%=String.format(Locale.US, "%.2f", entry.getPrezzoCorrente())%></td>
      <td><button onclick="addReview(<%= product.getId() %>, <%= id %>, <%= entry.getOrdine() %>)" id=addReviewButton>+</button></td>
    </tr>
    <%
                break;
              }
            }
          }
        }
      }
    %>
  </table>
  <div id="newRewiew" class="formAdder"></div>
  <%
    }
  %>
  <h1 class="userStuff" id="rewiewLabel">Recensioni</h1><br>
  <div id="reviewsContainer">
    <div class="reviewsDiv" id="reviewsDiv">
    </div>
  </div>
  <script>
    function addReview(productId, userId, orderId) {
      let editPanel = document.getElementById("newRewiew");
      editPanel.innerHTML = "";
      let string = "<form method=\"post\" action=\"ReviewsAdder\" class=\"container\">" +
              "<input type=\"hidden\" id=\"orderId" + productId + orderId + "\" name=\"orderId\" value=\"" + orderId + "\">" +
                  "<input type=\"hidden\" name=\"utente\" id=\"utente" + userId + "\" value=\"" + userId + "\">" +
                  "<input type=\"hidden\" name=\"prodotto\" id=\"utente" + productId + "\" value=\"" + productId + "\">" +
                  "<label for=\"stelle\">Stelle: </label>" +
                  "<input type=\"number\" id=\"stelle" + "\" name=\"stelle\" max=\"5\" min=\"1\" value=\"1\">" +
                  "<label for=\"recensione\">Descrizione: </label>" +
                  "<textarea name=\"recensione\" id=\"recensione" + userId + "\" cols=\"30\" rows=\"10\"></textarea>" +
                  "<input type=\"submit\" id=\"submitButtonReviews\" value=\"Aggiorna Prodotto\">" +
                  "</form>";
          editPanel.innerHTML += string;
    }
    
    function redirect() {
      window.location.replace("index.jsp");
    }

    if(<%=admin%>)
      $.ajax({
        url: "ProGetter?top=0&type=0",
        success: function (result){
          createProductTable(result);
        }

      })

    let products = null;

    function createProductTable(result){
      products = result;
      sessionStorage.setItem("products",products);
      let productTableHTML = "";
      document.getElementById("productsTable").innerHTML = "";
      for(let i = 0; i < products.length; i++){
        productTableHTML += "<tr id=" + products[i]["id"] + " class=\"productsRow\">";
        productTableHTML += "<td><img src=" + products[i]["immagine"] + " alt=\"image" + products[i]["id"] + "\" width=\"50px\"></td>";
        productTableHTML += "<td>" + "Product ID:" + products[i]["id"] + "</td>";
        productTableHTML += "<td>" + products[i]["nome"] + "</td>";
        productTableHTML += "<td>" +"<button onclick=\"editProduct("+ products[i]["id"]+")\"> Update</button>" + "</td>";
        productTableHTML += "<td>" +"<button onclick=\"removeProduct(" + products[i]["id"] +")\"> Delete</button>" + "</td>";
        productTableHTML += "</tr>";
      }
      productTableHTML += "<tr></tr><tr><td></td><td></td><td></td><td></td>" + "<td><button onclick=\"addProduct()\" id=\"addProductButton\">Aggiungi Prodotto</button></td>" + "</tr>";
      document.getElementById("productsTable").innerHTML = productTableHTML;
    }


    function removeProduct(id){
      $.ajax({
        url: "ProductEdit?id=" + id,
        success: function (){
          document.getElementById(id).innerHTML = "";
        }
      })
    }

    function editProduct(id){
      let rows = document.getElementsByClassName("productsRow");
      let selectedRow = document.getElementById(id);
      let editPanel = document.getElementById("editProducts");
      let addProductButton = document.getElementById("addProductButton");
      for(let j = 0; j < rows.length; j++) {
        rows[j].style.display = "none";
      }
      console.log(products);
      let k = 0;
      for(k = 0; k < products.length; k++){
        console.log(products[k]["id"]);
        if(products[k]["id"] == id){
              break;
        }
      }

      let miele = "";
      let attrezzature = "";
      let accessori = "";
      if (products[k]["tipo"] == 1) {
        miele = "selected";
      } else if (products[k]["tipo"] == 2) {
        attrezzature = "selected";
      } else if (products[k]["tipo"] == 3) {
        accessori = "selected";
      }
      addProductButton.style.display = "none"
      selectedRow.style.display = "block";

      let string = "<form method=\"post\" action=\"ProductEdit\" class=\"container\" enctype=\"multipart/form-data\">" +
              "<label for=\"immagine\">Immagine:</label> " +
              "<input type=\"file\" name=\"immagine\" id=\"immagine" + products[k]["id"] + "\" value=\"" + products[k]["immagine"] + "\">" +
              "<label for=\"tipo\">Tipo: </label>" +
              "<select name=\"tipo\" id=\"tipo" + products[k]["id"] + "\">" +
              "<option value=\"1\"" + miele + ">Miele</option>" +
              "<option value=\"2\"" + attrezzature + ">Attrezzature</option>" +
              "<option value=\"3\"" + accessori + ">Accessori</option>" +
              "</select>" +
              "<label for=\"nome\">Nome: </label>" +
              "<input type=\"text\" name=\"nome\" id=\"nome" + products[k]["id"] + "\" value=\"" + products[k]["nome"] + "\">" +
              "<label for=\"descrizione\">Descrizione: </label>" +
              "<textarea name=\"descrizione\" id=\"descrizione" + products[k]["id"] + "\" cols=\"30\" rows=\"10\">" + products[k]["descrizione"] + "</textarea>" +
              "<label for=\"costo\">Costo: </label>" +
              "<input type=\"number\" step=\"0.01\" id=\"costo" + products[k]["id"] + "\" name=\"costo\" value=\"" + products[k]["costo"] + "\">" +
              "<label for=\"sconto\">Sconto: </label>" +
              "<input type=\"number\" id=\"sconto" + products[k]["id"] + "\" name=\"sconto\" max=\"100\" min=\"0\" value=\"" + products[k]["sconto"] + "\">" +
              "<label for=\"quantita\">Quantit&agrave; Rimasta:</label>" +
              "<input type=\"number\" id=\"quantita" + products[k]["id"] + "\" name=\"quantita\" min=\"0\" value=\"" + products[k]["quantita"] + "\">" +
              "<input type=\"hidden\" id=\"numeroOrdini" + products[k]["id"] + "\" name=\"numeroOrdini\" value=\"" + products[k]["numeroOrdini"] + "\">" +
              "<input type=\"hidden\" id=\"pathOriginale" + products[k]["id"] + "\" name=\"pathOriginale\" value=\"" + products[k]["immagine"] + "\">" +
              "<input type=\"hidden\" id=\"id" + products[k]["id"] + "\" name=\"id\" value=\"" + products[k]["id"] + "\">" +
              "<input type=\"hidden\" id=\"edit" + products[k]["id"] + "\" name=\"edit\" value=\"true\">" +
              "<input type=\"submit\" id=\"submitButton\" value=\"Aggiorna Prodotto\">" +
              "</form>";
      editPanel.innerHTML += string;
    }

    function updateProduct(productId) {
      let newImmagine = "";
      let newTipo = 0;
      let newNome =  "";
      let newDescr =  "";
      let newCosto = 0.0;
      let newSconto = 0;
      let newQuantita = 0;
      let newNumeroOrdini = 0;
      let pathOriginale = "";

      newImmagine = document.getElementById("immagine" + productId).value;
      if(newImmagine == null || newImmagine === "" || newImmagine === " ") {
        newImmagine = document.getElementById("edit" + productId).value;
      }
      newTipo = document.getElementById("tipo" + productId).value;
      newNome = document.getElementById("nome" + productId).value;
      newDescr = document.getElementById("descrizione" + productId).value;
      newCosto = document.getElementById("costo" + productId).value;
      newSconto = document.getElementById("sconto" + productId).value;
      newQuantita = document.getElementById("quantita" + productId).value;
      newNumeroOrdini = document.getElementById("numeroOrdini" + productId).value;
      pathOriginale = document.getElementById("pathOriginale" + productId).value;

      console.log(newImmagine);
      console.log(newTipo);
      console.log(newNome);
      console.log(newDescr);
      console.log(newCosto);
      console.log(newSconto);
      console.log(newQuantita);
/*
      $.ajax({
      url: "ProductEdit?edit=true&id=" + productId,
              type: 'POST',
              data: { immagine: newImmagine, tipo : newTipo, nome: newNome, descrizione: newDescr, costo: newCosto, sconto: newSconto, quantita: newQuantita, numeroOrdini: newNumeroOrdini, pathOriginale: pathOriginale} ,
              success: function (){
              document.getElementById(id).innerHTML = "";
        }
      })

      $.post({
        url: "ProductEdit?edit=true&id=" + productId + "&immagine=" + newImmagine + "&tipo=" + newTipo +
        "&nome=" + newNome + "&descrizione=" + newDescr + "&costo=" + newCosto + "&sconto=" + newSconto  +
         "&quantita=" + newQuantita + "&numeroOrdini=" + newNumeroOrdini + "&pathOriginale" + pathOriginale,
        data: { immagine: newImmagine} ,
        contentType: 'multipart/mixed stream',
        success: function (){
          document.getElementById(id).innerHTML = "";
        }
      })
    */
    }

    function addProduct(){
      window.location.href="pageCreator.jsp";
    }


    reviewsGetter(<%= id %>);

    function reviewsGetter(id) {
      setElementID1("reviewsDiv");
      $.post({
        url:"ReviewsServlet?userPageRequest=" + id,
        success: function(result){
          createReviews(result);
        }
      })
    }

  </script>


    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <%@include file="WEB-INF/includes/footer.html"%><br>
  </body>
</html>
