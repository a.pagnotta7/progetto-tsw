package com.example.apicoltura_mieldoro.controller;

import com.example.apicoltura_mieldoro.model.utente.UtenteBean;
import com.example.apicoltura_mieldoro.model.utente.UtenteDAO;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "UserUpdate", value = "/UserUpdate")
public class UserUpdate extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String newName = "";
        String newCognome = "";
        String newEmail = "";
        String newPassword = "";
        if(request.getParameter("newName") != null  || request.getParameter("newName").equals("") || request.getParameter("newName").equals(" "))
            newName =  request.getParameter("newName");
        if(request.getParameter("newCognome") != null  || request.getParameter("newCognome").equals("") || request.getParameter("newCognome").equals(" "))
            newCognome =  request.getParameter("newCognome");
        if(request.getParameter("newEmail") != null  || request.getParameter("newEmail").equals("") || request.getParameter("newEmail").equals(" "))
            newEmail =  request.getParameter("newEmail");
        if(request.getParameter("newPassword") != null  || request.getParameter("newPassword").equals("") || request.getParameter("newPassword").equals(" "))
            newPassword =  request.getParameter("newPassword");
        UtenteDAO updater = new UtenteDAO();
        UtenteBean user = (UtenteBean) request.getSession().getAttribute("utente");
        if(newName != null  && !newName.equals("") && !newName.equals(" ")){
            user.setNome(newName);
            try {
                updater.doUpdate(user);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        if(newCognome != null  && !newCognome.equals("") && !newCognome.equals(" ")){
            user.setCognome(newCognome);
            try {
                updater.doUpdate(user);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        if(newEmail != null  && !newEmail.equals("") && !newEmail.equals(" ")){
            user.setEmail(newEmail);
            try {
                updater.doUpdate(user);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        if(newPassword != null  && !newPassword.equals("") && !newPassword.equals(" ")){
            user.setPassword(newPassword);
            try {
                updater.doUpdate(user);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }
}

