package com.example.apicoltura_mieldoro.controller;

import com.example.apicoltura_mieldoro.model.prodotto.ProdottoBean;
import com.example.apicoltura_mieldoro.model.prodotto.ProdottoDAO;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "ProductPage", value = "/ProductPage")
public class ProductPage extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = -1;
        HttpSession session = null;
        if(request.getParameter("id") != null) {
            id = Integer.parseInt(request.getParameter("id"));
            session = request.getSession();
            session.setAttribute("lastProduct",id);
        }else{
            session = request.getSession();
            id = (Integer) session.getAttribute("lastProduct");
        }
        ProdottoDAO retriever = new ProdottoDAO();
        List<ProdottoBean> productList;
        String json;
        try {
            int finalId = id;
            productList = retriever.doRetrieveOnCondition(prod -> prod.getId() == finalId);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        request.setAttribute("prodottoBean", productList.get(0));
        RequestDispatcher dispatcher = request.getRequestDispatcher("productPage.jsp");
        dispatcher.forward(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
