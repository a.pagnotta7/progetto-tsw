package com.example.apicoltura_mieldoro.controller;

import com.example.apicoltura_mieldoro.model.elementoOrdine.*;
import com.example.apicoltura_mieldoro.model.ordine.*;
import com.example.apicoltura_mieldoro.model.prodotto.*;
import com.example.apicoltura_mieldoro.model.utente.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;


import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "UserPage", value = "/UserPage")
public class UserPage extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        UtenteBean user = null;
        if(session.getAttribute("utente") == null){
            RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
            dispatcher.forward(request, response);
        }else{
            user = (UtenteBean) session.getAttribute("utente");
        }
        OrdineDAO orderRetriever = new OrdineDAO();
        ElementoOrdineDAO orderEntryRetriever = new ElementoOrdineDAO();
        ProdottoDAO productRetriever = new ProdottoDAO();
        try {
            UtenteBean finalUser = user;
            List<OrdineBean> orders = orderRetriever.doRetrieveOnCondition(order -> order.getCliente() == finalUser.getId());
            List<ElementoOrdineBean> entries = new ArrayList<>();
            List<ProdottoBean> products = productRetriever.doRetrieveAll();
            if(orders != null && !orders.isEmpty()) {
                for(OrdineBean order: orders) {
                    List<ElementoOrdineBean> buf = orderEntryRetriever.doRetrieveOnCondition(entry -> entry.getOrdine() == order.getId());
                    if(buf == null || buf.isEmpty())
                        throw new RuntimeException("no entries for order with id = " + order.getId());
                    entries.addAll(buf);
                }
                request.setAttribute("noOrders", false);
                request.setAttribute("noReviews", false);
                request.setAttribute("orders", orders);
                request.setAttribute("entries", entries);
                request.setAttribute("products", products);
                RequestDispatcher dispatcher = request.getRequestDispatcher("userPage.jsp");
                dispatcher.forward(request, response);
            }
            else {
                request.setAttribute("noOrders", true);
                request.setAttribute("noReviews", true);
                RequestDispatcher dispatcher = request.getRequestDispatcher("userPage.jsp");
                dispatcher.forward(request, response);
            }
        } catch (SQLException e) {
            throw new RuntimeException("something went wrong retrieving orders");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }
}


