package com.example.apicoltura_mieldoro.controller;

import com.example.apicoltura_mieldoro.model.prodotto.ProdottoBean;
import com.example.apicoltura_mieldoro.model.prodotto.ProdottoDAO;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Objects;

@MultipartConfig
@WebServlet(name = "ProductEdit", value = "/ProductEdit")
public class ProductEdit extends HttpServlet {
    private static final String path = "images";
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ProdottoBean bean;
        ProdottoDAO updater = new ProdottoDAO();
        int id;
        String pathOriginale = null;
        Boolean edit = Boolean.parseBoolean(request.getParameter("edit"));
        if(edit){
            id = Integer.parseInt(request.getParameter("id"));
            pathOriginale = (String) request.getParameter("pathOriginale");
            try {
                bean = updater.doRetrieveById(id);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }else{
            bean = new ProdottoBean();
        }
        String nome = (String) request.getParameter("nome");
        String descrizione = (String) request.getParameter("descrizione");
        String costo = (String) request.getParameter("costo");
        String sconto = (String) request.getParameter("sconto");
        String quantita = (String) request.getParameter("quantita");
        String tipo = (String) request.getParameter("tipo");
        String numeroOrdini =(String) request.getParameter("numeroOrdini");
        boolean errori = false;
        if(!nome.isEmpty()){
            if (nome.length() > 32 ) {
                errori = true;
            }
            else{
                bean.setNome(nome);
            }
        }
        if (!costo.isEmpty()) {
            double parsedCosto = Double.parseDouble(costo);
            if(parsedCosto < 0) {
                errori= true;
            }else{
                bean.setCosto(parsedCosto);
            }
        }
        if (!sconto.isEmpty()) {
            int parsedSconto = Integer.parseInt(sconto);
            if(parsedSconto < 0 || parsedSconto > 100){
                errori = true;
            }else{
                bean.setSconto(parsedSconto);
            }
        }
        if(!quantita.isEmpty()){
            int parsedQuantita = Integer.parseInt(quantita);
            if(parsedQuantita < 0){
                errori = true;
            }else{
                bean.setQuantita(parsedQuantita);
            }
        }
        if(!tipo.isEmpty()){
            int parsedTipo = Integer.parseInt(tipo);
            bean.setTipo(parsedTipo);
        }
        if(!descrizione.isEmpty()){
            bean.setDescrizione(descrizione);
        }
        if (errori) {
            request.setAttribute("errorflag", true);
            response.sendError(501);
        }
        Part filePart = request.getPart("immagine");
        String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
        String destinazione = null;
        if (!fileName.isEmpty()) {
            String subdirectory;
            if(Objects.requireNonNull(tipo) == "1"){

                subdirectory = "mieli";
            } else if (Objects.requireNonNull(tipo) == "2") {
                subdirectory = "attrezzature";
            } else if (Objects.requireNonNull(tipo) == "3") {
                subdirectory = "accessori";
            }else{
                subdirectory = "";
            }
            destinazione = path + "/" + subdirectory + "/" + fileName;
            Path pathDestinazione = Paths.get(getServletContext().getRealPath(destinazione));
            for (int i = 2; Files.exists(pathDestinazione); i++) {
                destinazione = path + "/" + subdirectory + "/" + i + "_" + fileName;
                pathDestinazione = Paths.get(getServletContext().getRealPath(destinazione));
            }
            InputStream fileInputStream = filePart.getInputStream();
            Files.createDirectories(pathDestinazione.getParent());
            Files.copy(fileInputStream, pathDestinazione);
            bean.setImmagine(destinazione);
        }
        else{
            destinazione = pathOriginale;
        }
        if(edit){
            try {
                updater.doUpdate(bean);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }else{
            try {
                updater.doSave(bean);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher("UserPage");
        dispatcher.forward(request,response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        ProdottoDAO prodottoDAO = new ProdottoDAO();
        ProdottoBean bean = null;
        try {
            bean = prodottoDAO.doRetrieveById(id);
            prodottoDAO.doDelete(bean);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
