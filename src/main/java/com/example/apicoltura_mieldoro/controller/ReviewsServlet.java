package com.example.apicoltura_mieldoro.controller;

import com.example.apicoltura_mieldoro.model.prodotto.ProdottoBean;
import com.example.apicoltura_mieldoro.model.prodotto.ProdottoDAO;
import com.example.apicoltura_mieldoro.model.recensione.RecensioneBean;
import com.example.apicoltura_mieldoro.model.recensione.RecensioneDAO;
import com.example.apicoltura_mieldoro.model.utente.UtenteBean;
import com.example.apicoltura_mieldoro.model.utente.UtenteDAO;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet(name = "ReviewsServlet", value = "/ReviewsServlet")

public class ReviewsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        UtenteBean user = null;
        List<RecensioneBean> recensioni = null;
        RecensioneDAO retriever = new RecensioneDAO();
            try {
                UtenteBean finalUser = user;
                if(request.getParameter("userPageRequest") != null && request.getParameter("userPageRequest") != "" && request.getParameter("userPageRequest") != " "){
                    if(session.getAttribute("utente") == null){
                        RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
                        dispatcher.forward(request, response);
                    }else {
                        user = (UtenteBean) session.getAttribute("utente");
                        int id = user.getId();
                        recensioni = retriever.doRetrieveOnCondition(rec -> rec.getUtente() == id);
                        int recensioniSize = recensioni.size();
                        //int recensioniSize;
                        //recensioniSize = recensioni.size();
                        String res = "[";
                        for(int i = 0; i < recensioniSize; i++)
                        {
                            if(i == 0)
                            {
                                int prodId = recensioni.get(i).getProdotto();
                                ProdottoDAO prodRetriever = new ProdottoDAO();
                                ProdottoBean reviewProduct = new ProdottoBean();
                                reviewProduct.setId(prodId);
                                List<ProdottoBean> c;
                                try {
                                    c = prodRetriever.doRetrieveOnCondition(prodottoBean -> prodottoBean.getId() == reviewProduct.getId());
                                } catch (SQLException e) {
                                    throw new RuntimeException("something went wrong retrieving reviews");
                                }
                                if(c.size() != 1)
                                    throw new RuntimeException("something went wrong retrieving reviews");
                                ProdottoBean prodotto = c.get(0);
                                String name = prodotto.getNome();
                                res += recensioni.get(i).stringify();
                                res = res.replace("\"prodotto\": "+ recensioni.get(i).getProdotto() + ",", "\"prodotto\": \""+ name + "\",");
                            } else {
                                res += ", " + recensioni.get(i).stringify();
                                int prodId = recensioni.get(i).getProdotto();
                                ProdottoDAO prodRetriever = new ProdottoDAO();
                                ProdottoBean reviewProduct = new ProdottoBean();
                                reviewProduct.setId(prodId);
                                List<ProdottoBean> c;
                                try {
                                    c = prodRetriever.doRetrieveOnCondition(prodottoBean -> prodottoBean.getId() == reviewProduct.getId());
                                } catch (SQLException e) {
                                    throw new RuntimeException("something went wrong retrieving reviews");
                                }
                                if (c.size() != 1)
                                    throw new RuntimeException("something went wrong retrieving reviews");
                                ProdottoBean prodotto = c.get(0);
                                String name = prodotto.getNome();
                                res = res.replace("\"prodotto\": " + recensioni.get(i).getProdotto() + ",", "\"prodotto\": \"" + name + "\",");
                            }
                        }
                        res += "]";
                        response.setContentType("application/json");
                        PrintWriter out = response.getWriter();
                        out.println(res);
                    }
                }else {
                    recensioni = retriever.doRetrieveAll();
                    if (request.getParameter("prodottoRec") != null && request.getParameter("prodottoRec") != "" && request.getParameter("prodottoRec") != " ") {
                        int id = Integer.parseInt(request.getParameter("prodottoRec"));
                        recensioni = recensioni.stream().filter(rec -> rec.getProdotto() == id).collect(Collectors.toList());
                        int recensioniSize;
                        recensioniSize = recensioni.size();
                        String res = "[";
                        for(int i = 0; i < recensioniSize; i++)
                        {
                            if(i == 0)
                            {
                                    int userId = recensioni.get(i).getUtente();
                                    UtenteDAO userRetriever = new UtenteDAO();
                                    UtenteBean reviewUser = new UtenteBean();
                                    reviewUser.setId(userId);
                                    List<UtenteBean> c;
                                    try {
                                        c = userRetriever.doRetrieveOnCondition(utenteBean -> utenteBean.getId() == reviewUser.getId());
                                    } catch (SQLException e) {
                                        throw new RuntimeException("something went wrong retrieving reviews");
                                    }
                                    if(c.size() != 1)
                                        throw new RuntimeException("something went wrong retrieving reviews");
                                    UtenteBean utente = c.get(0);
                                    String name = utente.getNome();
                                res += recensioni.get(i).stringify();
                                res = res.replace("\"utente\": "+ recensioni.get(i).getUtente() + ",", "\"utente\": \""+ name + "\",");
                            } else
                            {
                                res += ", " + recensioni.get(i).stringify();
                                int userId = recensioni.get(i).getUtente();
                                UtenteDAO userRetriever = new UtenteDAO();
                                UtenteBean reviewUser = new UtenteBean();
                                reviewUser.setId(userId);
                                List<UtenteBean> c;
                                try {
                                    c = userRetriever.doRetrieveOnCondition(utenteBean -> utenteBean.getId() == reviewUser.getId());
                                } catch (SQLException e) {
                                    throw new RuntimeException("something went wrong retrieving reviews");
                                }
                                if(c.size() != 1)
                                    throw new RuntimeException("something went wrong retrieving reviews");
                                UtenteBean utente = c.get(0);
                                String name = utente.getNome();
                                res = res.replace("\"utente\": "+ recensioni.get(i).getUtente() + ",", "\"utente\": \""+ name + "\",");
                            }
                        }
                        res += "]";
                        response.setContentType("application/json");
                        PrintWriter out = response.getWriter();
                        out.println(res);
                    }
                }
            } catch (SQLException e) {
                throw new RuntimeException("something went wrong retrieving reviews");
            }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }
}
