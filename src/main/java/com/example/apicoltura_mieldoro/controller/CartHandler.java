package com.example.apicoltura_mieldoro.controller;

import com.example.apicoltura_mieldoro.model.carrello.*;
import com.example.apicoltura_mieldoro.model.prodotto.*;
import com.example.apicoltura_mieldoro.model.utente.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "CartHandler", value = "/CartHandler")
public class CartHandler extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String mode = request.getParameter("mode");
        HttpSession session = request.getSession();
        List<CartEntry> cart = (List<CartEntry>) session.getAttribute("carrello");
        int itemId = Integer.parseInt(request.getParameter("id"));
        UtenteBean user = (UtenteBean) session.getAttribute("utente");
        boolean isLoggedIn = Boolean.parseBoolean((String) session.getAttribute("isLoggedIn"));
        CarrelloDAO saver = new CarrelloDAO();
        int quantity = 1;
        if(request.getParameter("quantity") != null)
            quantity = Integer.parseInt(request.getParameter("quantity"));
        if(mode.equalsIgnoreCase("add")){
                if (cart == null) {
                    cart = new ArrayList<>();
                }
                for(CartEntry entry: cart) {
                    if(entry.getEntry().getProdotto() == itemId) {
                        ProdottoDAO prodDAO = new ProdottoDAO();
                        ProdottoBean prod;
                        try {
                            prod = prodDAO.doRetrieveById(entry.getEntry().getProdotto());
                        } catch (SQLException e) {
                            throw new RuntimeException(e);
                        }
                        if(entry.getEntry().getQuantita() + quantity <= prod.getQuantita())
                            entry.getEntry().setQuantita(entry.getEntry().getQuantita() + quantity);
                        else if (entry.getEntry().getQuantita() + quantity > prod.getQuantita())
                            entry.getEntry().setQuantita(entry.getEntry().getQuantita() + (prod.getQuantita() - entry.getEntry().getQuantita()));
                            request.setAttribute("outcome", "false");
                        session.setAttribute("carrello", cart);
                        if(isLoggedIn){
                            try {
                                List<CarrelloBean> check = saver.doRetrieveOnCondition(bean -> bean.getCliente() == entry.getEntry().getCliente() && bean.getProdotto() == entry.getEntry().getProdotto());
                                if(!check.isEmpty())
                                    saver.doUpdate(entry.getEntry());
                                else
                                    saver.doSave(entry.getEntry());
                            } catch (SQLException e) {
                                throw new RuntimeException(e);
                            }
                        }
                        return;
                    }
                }
                ProdottoDAO retriever = new ProdottoDAO();
                try {
                    CarrelloBean entry = new CarrelloBean(user == null ? -1 : user.getId(), itemId, quantity);
                    cart.add(new CartEntry(
                            entry,
                            retriever.doRetrieveOnCondition(prodotto -> prodotto.getId() == itemId).get(0)
                    ));
                    if(isLoggedIn) {
                        try {
                            List<CarrelloBean> check = saver.doRetrieveOnCondition(bean -> bean.getCliente() == entry.getCliente() && bean.getProdotto() == entry.getProdotto());
                            if(!check.isEmpty())
                                saver.doUpdate(entry);
                            else
                                saver.doSave(entry);
                        } catch (SQLException e) {
                            throw new RuntimeException(e);
                        }
                    }
                } catch (SQLException ignored) {
                }

                session.setAttribute("carrello", cart);
            }
        if(mode.equalsIgnoreCase("sub")){
                if (cart == null) return;

                for(int i = 0; i < cart.size(); i++) {
                    if(cart.get(i).getEntry().getProdotto() == itemId) {
                        cart.get(i).getEntry().setQuantita(cart.get(i).getEntry().getQuantita() - 1);
                        if(cart.get(i).getEntry().getQuantita() <= 0) {
                            try {
                                CarrelloDAO cartDel = new CarrelloDAO();
                                cartDel.doDelete(cart.get(i).getEntry());
                            } catch (SQLException ignored) {
                            }
                            cart.remove(i);
                            session.setAttribute("carrello", cart);
                            return;
                        }
                        try {
                            saver.doUpdate(cart.get(i).getEntry());
                        } catch (SQLException ignored) {
                        }
                        break;
                    }
                }

                session.setAttribute("carrello", cart);
            }
        if(mode.equalsIgnoreCase("remove")){
                if (cart == null) return;

                for(int i = 0; i < cart.size(); i++) {
                    if(cart.get(i).getEntry().getProdotto() == itemId) {
                        try {
                            CarrelloDAO cartDel = new CarrelloDAO();
                            cartDel.doDelete(cart.get(i).getEntry());
                        } catch (SQLException ignored) {
                        }
                        cart.remove(i);
                        session.setAttribute("carrello", cart);
                        return;
                    }
                }
                session.setAttribute("carrello", cart);
            }
        if(mode.equalsIgnoreCase("save")){
                try {
                    if(isLoggedIn){
                        CarrelloUtils.doSave(cart);
                    }
                } catch (SQLException ignored) {
                }
                session.setAttribute("carrello", cart);
            }
        RequestDispatcher dispatcher = request.getRequestDispatcher("carrello.jsp");
        dispatcher.forward(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
