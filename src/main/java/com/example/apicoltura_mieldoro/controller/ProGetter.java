package com.example.apicoltura_mieldoro.controller;

import com.example.apicoltura_mieldoro.model.prodotto.ProdottoBean;
import com.example.apicoltura_mieldoro.model.prodotto.ProdottoDAO;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;


import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet(name = "ProGetter", value = "/ProGetter")
public class ProGetter extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int type = Integer.parseInt(request.getParameter("type"));
        int top = Integer.parseInt(request.getParameter("top"));
        int discount = 0;
        if(request.getParameter("discount") != null)
            discount = Integer.parseInt(request.getParameter("discount"));
        int price = 0;
        if(request.getParameter("price") != null)
            price = Integer.parseInt(request.getParameter("price"));
        String searchString;
        if(request.getParameter("searchString") != null)
            searchString = (String) request.getParameter("searchString");
        else {
            searchString = "";
        }
        ProdottoDAO retriever = new ProdottoDAO();
        List<ProdottoBean> products = null;
        try {
            if(type == 0) {
                if (searchString != null && !searchString.equals("") && !searchString.equals(" ")) {
                    products = retriever.doRetrieveOnCondition(prod -> prod.getNome().toLowerCase().contains(searchString.toLowerCase()));
                    if (discount != 0) {
                        if (discount == 1)
                            products = products.stream().filter(prod -> (prod.getSconto() > 5) && (prod.getSconto() <= 15)).collect(Collectors.toList());
                        else if (discount == 2)
                            products = products.stream().filter(prod -> (prod.getSconto() > 15) && (prod.getSconto() <= 30)).collect(Collectors.toList());
                        else if (discount == 3)
                            products = products.stream().filter(prod -> (prod.getSconto() > 30) && (prod.getSconto() <= 45)).collect(Collectors.toList());
                        else if (discount == 4)
                            products = products.stream().filter(prod -> (prod.getSconto() > 45) && (prod.getSconto() <= 60)).collect(Collectors.toList());
                    }
                    if (price != 0) {
                        if (price == 1)
                            products = products.stream().filter(prod -> (prod.getCosto() > 5) && (prod.getCosto() <= 20)).collect(Collectors.toList());
                        else if (price == 2)
                            products = products.stream().filter(prod -> (prod.getCosto() > 20) && (prod.getCosto() <= 30)).collect(Collectors.toList());
                        else if (price == 3)
                            products = products.stream().filter(prod -> (prod.getCosto() > 30) && (prod.getCosto() <= 40)).collect(Collectors.toList());
                        else if (price == 4)
                            products = products.stream().filter(prod -> (prod.getCosto() > 40)).collect(Collectors.toList());
                    }
                } else {
                    products = retriever.doRetrieveAll();
                    if (discount != 0) {
                        if (discount == 1)
                            products = products.stream().filter(prod -> (prod.getSconto() > 5) && (prod.getSconto() <= 15)).collect(Collectors.toList());
                        else if (discount == 2)
                            products = products.stream().filter(prod -> (prod.getSconto() > 15) && (prod.getSconto() <= 30)).collect(Collectors.toList());
                        else if (discount == 3)
                            products = products.stream().filter(prod -> (prod.getSconto() > 30) && (prod.getSconto() <= 45)).collect(Collectors.toList());
                        else if (discount == 4)
                            products = products.stream().filter(prod -> (prod.getSconto() > 45) && (prod.getSconto() <= 60)).collect(Collectors.toList());
                    }
                    if (price != 0) {
                        if (price == 1)
                            products = products.stream().filter(prod -> (prod.getCosto() > 5) && (prod.getCosto() <= 20)).collect(Collectors.toList());
                        else if (price == 2)
                            products = products.stream().filter(prod -> (prod.getCosto() > 20) && (prod.getCosto() <= 30)).collect(Collectors.toList());
                        else if (price == 3)
                            products = products.stream().filter(prod -> (prod.getCosto() > 30) && (prod.getCosto() <= 40)).collect(Collectors.toList());
                        else if (price == 4)
                            products = products.stream().filter(prod -> (prod.getCosto() > 40)).collect(Collectors.toList());
                    }
                }
            }else{
                if(searchString != null && !searchString.equals("") && !searchString.equals(" ")){
                    products = retriever.doRetrieveOnCondition(prod -> prod.getTipo() == type && prod.getNome().toLowerCase().contains(searchString.toLowerCase()));
                    if (discount != 0) {
                        if (discount == 1)
                            products = products.stream().filter(prod -> (prod.getSconto() > 5) && (prod.getSconto() <= 15)).collect(Collectors.toList());
                        else if (discount == 2)
                            products = products.stream().filter(prod -> (prod.getSconto() > 15) && (prod.getSconto() <= 30)).collect(Collectors.toList());
                        else if (discount == 3)
                            products = products.stream().filter(prod -> (prod.getSconto() > 30) && (prod.getSconto() <= 45)).collect(Collectors.toList());
                        else if (discount == 4)
                            products = products.stream().filter(prod -> (prod.getSconto() > 45) && (prod.getSconto() <= 60)).collect(Collectors.toList());
                    }
                    if (price != 0) {
                        if (price == 1)
                            products = products.stream().filter(prod -> (prod.getCosto() > 5) && (prod.getCosto() <= 20)).collect(Collectors.toList());
                        else if (price == 2)
                            products = products.stream().filter(prod -> (prod.getCosto() > 20) && (prod.getCosto() <= 30)).collect(Collectors.toList());
                        else if (price == 3)
                            products = products.stream().filter(prod -> (prod.getCosto() > 30) && (prod.getCosto() <= 40)).collect(Collectors.toList());
                        else if (price == 4)
                            products = products.stream().filter(prod -> (prod.getCosto() > 40)).collect(Collectors.toList());
                    }
                }else{
                    products = retriever.doRetrieveOnCondition(prod -> prod.getTipo() == type);
                    if (discount != 0) {
                        if (discount == 1)
                            products = products.stream().filter(prod -> (prod.getSconto() > 5) && (prod.getSconto() <= 15)).collect(Collectors.toList());
                        else if (discount == 2)
                            products = products.stream().filter(prod -> (prod.getSconto() > 15) && (prod.getSconto() <= 30)).collect(Collectors.toList());
                        else if (discount == 3)
                            products = products.stream().filter(prod -> (prod.getSconto() > 30) && (prod.getSconto() <= 45)).collect(Collectors.toList());
                        else if (discount == 4)
                            products = products.stream().filter(prod -> (prod.getSconto() > 45) && (prod.getSconto() <= 60)).collect(Collectors.toList());
                    }
                    if (price != 0) {
                        if (price == 1)
                            products = products.stream().filter(prod -> (prod.getCosto() > 5) && (prod.getCosto() <= 20)).collect(Collectors.toList());
                        else if (price == 2)
                            products = products.stream().filter(prod -> (prod.getCosto() > 20) && (prod.getCosto() <= 30)).collect(Collectors.toList());
                        else if (price == 3)
                            products = products.stream().filter(prod -> (prod.getCosto() > 30) && (prod.getCosto() <= 40)).collect(Collectors.toList());
                        else if (price == 4)
                            products = products.stream().filter(prod -> (prod.getCosto() > 40)).collect(Collectors.toList());
                    }
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        int productsSize;
        if(top == 0)
        {
            productsSize = products.size();
        }
        else
        {
            productsSize = top;
            products.sort(Comparator.comparingDouble(ProdottoBean::getCosto));
        }
        String res = "[";
        for(int i = 0; i < productsSize; i++)
        {
            if(i == 0)
            {
                res += products.get(i).stringify();
            } else
            {
                res += ", " + products.get(i).stringify();
            }
        }
        res += "]";
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        out.println(res);
        /*if(request.getParameter("").equalsIgnoreCase("")){
            RequestDispatcher dispatcher = request.getRequestDispatcher("products.jsp");
            dispatcher.forward(request,response);
        }*/
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
