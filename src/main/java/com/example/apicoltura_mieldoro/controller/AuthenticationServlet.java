package com.example.apicoltura_mieldoro.controller;

import com.example.apicoltura_mieldoro.model.carrello.*;
import com.example.apicoltura_mieldoro.model.prodotto.*;
import com.example.apicoltura_mieldoro.model.utente.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

@WebServlet(name = "AuthenticationServlet", value = "/AuthenticationServlet")
public class AuthenticationServlet extends HttpServlet{
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String type = request.getParameter("type");
        boolean outcome = type.equals("login") ? login(request) : register(request);
        if(outcome) {
            RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
            HttpSession session = request.getSession();
            UtenteBean user = (UtenteBean) session.getAttribute("utente");
            List<CartEntry> cart = (List<CartEntry>) session.getAttribute("carrello");
            List<CarrelloBean> entries;
            CarrelloDAO entryRetriever = new CarrelloDAO();
            ProdottoDAO productRetriever = new ProdottoDAO();
            if(cart != null && !cart.isEmpty()) {
                for(CartEntry entry: cart)
                    entry.getEntry().setCliente(user.getId());

                try {
                    entries = entryRetriever.doRetrieveOnCondition(entry -> entry.getCliente() == user.getId());
                    boolean found;
                    for(CarrelloBean entry: entries) {
                        found = false;
                        for(CartEntry cartEntry: cart) {
                            if(cartEntry.getEntry().getProdotto() == entry.getProdotto()) {
                                cartEntry.getEntry().setQuantita(cartEntry.getEntry().getQuantita() + entry.getQuantita());
                                found = true;
                                break;
                            }
                        }
                        if(found) continue;
                        cart.add(new CartEntry(entry, productRetriever.doRetrieveById(entry.getProdotto())));
                    }
                    CarrelloUtils.doSave(cart);
                } catch (SQLException ignored) {
                    outcome = false;
                }
            }else{
                if(cart == null){
                    cart = new ArrayList<>();
                }
                try {
                    entries = entryRetriever.doRetrieveOnCondition(entry -> entry.getCliente() == user.getId());
                    for(CarrelloBean bean: entries){
                        cart.add(new CartEntry(bean,productRetriever.doRetrieveById(bean.getProdotto())));
                    }
                } catch (SQLException e) {
                    outcome = false;
                    throw new RuntimeException(e);
                }

            }
            session.setAttribute("carrello", cart);

            dispatcher.forward(request, response);
        }
        else {
            String resource = type.equals("login") ? "login.jsp" : "subscription.jsp";
            request.setAttribute("outcome", "false");
            RequestDispatcher dispatcher = request.getRequestDispatcher(resource);
            dispatcher.forward(request, response);
        }
    }

    private boolean login(HttpServletRequest request) {
        UtenteDAO retriever = new UtenteDAO();
        final String email = request.getParameter("email");
        final String password = request.getParameter("password");
        UtenteBean user = new UtenteBean();
        user.setEmail(email);
        user.setPassword(password);
        List<UtenteBean> c;
        try {
            c = retriever.doRetrieveOnCondition(cliente -> cliente.getEmail().toLowerCase().equals(user.getEmail().toLowerCase()) && cliente.getPassword().equals(user.getPassword()));
        } catch (SQLException e) {
            return false;
        }
        if(c.size() != 1)
            return false;
        UtenteBean utente = c.get(0);
        String name = utente.getNome();
        HttpSession session = request.getSession();
        session.setAttribute("isLoggedIn", "true");
        session.setAttribute("utente", utente);
        return true;
    }

    private boolean register(HttpServletRequest request) {
        final String email = request.getParameter("email");
        final String password = request.getParameter("password");
        final String nome = request.getParameter("firstname");
        final String cognome = request.getParameter("lastname");
        UtenteBean cliente = new UtenteBean(-1,email, password, nome, cognome,false);
        if(!validate(cliente)) {
            return false;
        }
        cliente.setPassword(password);
        UtenteDAO saver = new UtenteDAO();
        List<UtenteBean> c;
        try {
            saver.doSave(cliente);
            c = saver.doRetrieveOnCondition(user -> user.getEmail().equals(cliente.getEmail()));
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            return false;
        }
        if(c.size() == 0){
            throw new RuntimeException();
        }
        HttpSession session = request.getSession();
        session.setAttribute("isLoggedIn", "true");
        session.setAttribute("utente", c.get(0));
        return true;
    }

    private boolean validate(UtenteBean user) {
        String email = user.getEmail();
        String password = user.getPassword();
        boolean emailMatches = Pattern.compile("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$").matcher(email).matches();
        boolean passwordMatches = Pattern.compile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+").matcher(password).matches();
        return !email.isEmpty() && emailMatches && email.length() <= 255 && passwordMatches && password.length() >= 8 && password.length() <= 32 && user.getNome().length() <= 24 && user.getCognome().length() <= 24;
    }

}
