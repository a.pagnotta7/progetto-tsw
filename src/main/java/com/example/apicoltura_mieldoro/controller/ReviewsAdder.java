package com.example.apicoltura_mieldoro.controller;


import com.example.apicoltura_mieldoro.model.ordine.OrdineBean;
import com.example.apicoltura_mieldoro.model.ordine.OrdineDAO;
import com.example.apicoltura_mieldoro.model.prodotto.ProdottoBean;
import com.example.apicoltura_mieldoro.model.prodotto.ProdottoDAO;
import com.example.apicoltura_mieldoro.model.recensione.RecensioneBean;
import com.example.apicoltura_mieldoro.model.recensione.RecensioneDAO;
import com.example.apicoltura_mieldoro.model.utente.UtenteBean;
import com.example.apicoltura_mieldoro.model.utente.UtenteDAO;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.sql.Date;

@WebServlet(name = "ReviewsAdder", value = "/ReviewsAdder")

public class ReviewsAdder extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RecensioneBean recensioneBean = new RecensioneBean();
        RecensioneDAO updater = new RecensioneDAO();
        OrdineDAO ordineDAO = new OrdineDAO();
        List<OrdineBean> orders = null;
        int utente = 0;
        int prodotto = 0;
        int stelle = 0;
        int orderId = 0;
        String recensione = "";
        if(request.getParameter("utente") != null)
            utente = Integer.parseInt(request.getParameter("utente"));
        if(request.getParameter("prodotto") != null)
            prodotto = Integer.parseInt(request.getParameter("prodotto"));
        if(request.getParameter("stelle") != null)
            stelle = Integer.parseInt(request.getParameter("stelle"));
        if(request.getParameter("orderId") != null)
            orderId = Integer.parseInt(request.getParameter("orderId"));
        if(request.getParameter("recensione") != null && request.getParameter("recensione") != "" && request.getParameter("recensione") != " ")
            recensione = (String) request.getParameter("recensione");
        System.out.println(utente);
        System.out.println(prodotto);
        System.out.println(stelle);
        System.out.println(orderId);
        System.out.println(recensione);
        try{
            int finalOrderId = orderId;
            orders = ordineDAO.doRetrieveOnCondition(ord -> ord.getId() == finalOrderId);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        if(orders != null && orders.size() == 1){
            boolean errorFlag = false;
            Date data = orders.get(0).getDataAcquisto();
            recensioneBean.setUtente(utente);
            recensioneBean.setProdotto(prodotto);
            recensioneBean.setDataAcquisto(data);
            if(stelle >= 1 && stelle <=5){
                recensioneBean.setStelle(stelle);
            }else{
                errorFlag = true;
            }
            if(!recensione.isEmpty() && recensione != null){
                recensioneBean.setRecensione(recensione);
            }else{
                errorFlag = true;
            }
            if (errorFlag) {
                request.setAttribute("errorflag", true);
                response.sendError(501);
            }
            try {
                updater.doSave(recensioneBean);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher("UserPage");
        dispatcher.forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }
}
