package com.example.apicoltura_mieldoro.model.utility;

import org.apache.tomcat.jdbc.pool.PoolProperties;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class DBConnector {
    private static DataSource datasource;
    private static final String jdbcURL = "jdbc:mysql://localhost:3306/apicoltura_miel_db";
    private static Connection conn = null;

    public static Connection getConnection() throws SQLException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            // Load the driver.
            conn = DriverManager.getConnection(jdbcURL, "root", "Stellamitica2002");
        } catch (SQLException e) {
            System.out.println("Connessione Fallita \n");
            System.out.println(e);
        } catch (ClassNotFoundException e) {
            System.out.println("DB driver not found \n");
            System.out.println(e);
        }
        return conn;
    }
}

