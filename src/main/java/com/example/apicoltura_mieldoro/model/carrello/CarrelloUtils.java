package com.example.apicoltura_mieldoro.model.carrello;
import java.sql.SQLException;
import java.util.List;
public class CarrelloUtils {
    public static void doSave(List<CartEntry> cart) throws SQLException {
        CarrelloDAO saver = new CarrelloDAO();
        for(CartEntry entry: cart) {
            try {
                List<CarrelloBean> check = saver.doRetrieveOnCondition(bean -> bean.getCliente() == entry.getEntry().getCliente() && bean.getProdotto() == entry.getEntry().getProdotto());
                if(!check.isEmpty())
                    saver.doUpdate(entry.getEntry());
                else
                    saver.doSave(entry.getEntry());
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }
}

