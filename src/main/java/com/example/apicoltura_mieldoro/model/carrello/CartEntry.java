package com.example.apicoltura_mieldoro.model.carrello;

import com.example.apicoltura_mieldoro.model.prodotto.ProdottoBean;

public class CartEntry {
    private CarrelloBean entry;
    private ProdottoBean prodotto;

    public CartEntry(){}

    public CartEntry(CarrelloBean entry, ProdottoBean prodotto) {
        this.entry = entry;
        this.prodotto = prodotto;
    }

    public CarrelloBean getEntry() {
        return entry;
    }

    public void setEntry(CarrelloBean entry) {
        this.entry = entry;
    }

    public ProdottoBean getProdotto() {
        return prodotto;
    }

    public void setProdotto(ProdottoBean prodotto) {
        this.prodotto = prodotto;
    }
}

