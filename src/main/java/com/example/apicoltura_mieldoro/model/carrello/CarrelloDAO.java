package com.example.apicoltura_mieldoro.model.carrello;

import com.example.apicoltura_mieldoro.model.utility.Condition;
import com.example.apicoltura_mieldoro.model.utility.DAO;
import com.example.apicoltura_mieldoro.model.utility.DBConnector;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CarrelloDAO implements DAO<CarrelloBean> {

    public CarrelloDAO(){}

    @Override
    public void doSave(CarrelloBean element) throws SQLException {
        Connection conn = DBConnector.getConnection();

        PreparedStatement statement = conn.prepareStatement("insert into carrello values (?, ?, ?)");
        statement.setInt(1, element.getCliente());
        statement.setInt(2, element.getProdotto());
        statement.setInt(3, element.getQuantita());

        statement.executeUpdate();

        statement.close();
        conn.close();
    }

    @Override
    public void doDelete(CarrelloBean element) throws SQLException {
        Connection conn = DBConnector.getConnection();

        PreparedStatement statement = conn.prepareStatement("delete from carrello where utente = ? and prodotto = ?");
        statement.setInt(1, element.getCliente());
        statement.setInt(2, element.getProdotto());

        statement.executeUpdate();

        statement.close();
        conn.close();
    }

    @Override
    public void doUpdate(CarrelloBean element) throws SQLException {
        Connection conn = DBConnector.getConnection();

        PreparedStatement statement = conn.prepareStatement("update carrello set utente = ?, prodotto = ?, quantita = ? where utente = ? and prodotto = ?");
        statement.setInt(1, element.getCliente());
        statement.setInt(2, element.getProdotto());
        statement.setInt(3, element.getQuantita());
        statement.setInt(4, element.getCliente());
        statement.setInt(5, element.getProdotto());

        statement.executeUpdate();

        statement.close();
        conn.close();
    }

    @Override
    public List<CarrelloBean> doRetrieveOnCondition(Condition<CarrelloBean> cond) throws SQLException {
        List<CarrelloBean> entries = new ArrayList<>();
        Connection conn = DBConnector.getConnection();

        Statement statement = conn.createStatement();
        ResultSet set = statement.executeQuery("select * from carrello");

        while(set.next()) {
            int cliente = set.getInt("utente");
            int prodotto = set.getInt("prodotto");
            int quantita = set.getInt("quantita");
            CarrelloBean entry = new CarrelloBean(cliente, prodotto, quantita);

            if(cond.test(entry))
                entries.add(entry);
        }

        statement.close();
        conn.close();
        return entries;
    }

    @Override
    public List<CarrelloBean> doRetrieveAll() throws SQLException {
        List<CarrelloBean> entries = new ArrayList<>();
        Connection conn = DBConnector.getConnection();

        Statement statement = conn.createStatement();
        ResultSet set = statement.executeQuery("select * from carrello");

        while(set.next()) {
            int cliente = set.getInt("utente");
            int prodotto = set.getInt("prodotto");
            int quantita = set.getInt("quantita");
            entries.add(new CarrelloBean(cliente, prodotto, quantita));
        }

        statement.close();
        conn.close();
        return entries;
    }
}

