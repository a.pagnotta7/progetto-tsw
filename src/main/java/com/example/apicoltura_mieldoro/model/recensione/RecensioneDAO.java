package com.example.apicoltura_mieldoro.model.recensione;

import com.example.apicoltura_mieldoro.model.carrello.CarrelloBean;
import com.example.apicoltura_mieldoro.model.ordine.OrdineBean;
import com.example.apicoltura_mieldoro.model.utente.UtenteBean;
import com.example.apicoltura_mieldoro.model.utility.Condition;
import com.example.apicoltura_mieldoro.model.utility.DAO;
import com.example.apicoltura_mieldoro.model.utility.DBConnector;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RecensioneDAO  implements DAO<RecensioneBean>{
    public RecensioneDAO() {}

    @Override
    public void doSave(RecensioneBean element) throws SQLException {
        Connection conn = DBConnector.getConnection();

        PreparedStatement statement = conn.prepareStatement("insert into recensioni (utente, prodotto, data_acquisto, stelle, recensione) values (?, ?, ?, ?, ?)");
        statement.setInt(1, element.getUtente());
        statement.setInt(2, element.getProdotto());
        statement.setDate(3, element.getDataAcquisto());
        statement.setInt(4, element.getStelle());
        statement.setString(5, element.getRecensione());

        statement.executeUpdate();

        statement.close();
        conn.close();
    }

    @Override
    public void doDelete(RecensioneBean element) throws SQLException {
        Connection conn = DBConnector.getConnection();

        PreparedStatement statement = conn.prepareStatement("delete from recensioni where id = ?");
        statement.setInt(1, element.getId());

        statement.executeUpdate();

        statement.close();
        conn.close();
    }

    @Override
    public void doUpdate(RecensioneBean element) throws SQLException {
        Connection conn = DBConnector.getConnection();

        PreparedStatement statement = conn.prepareStatement("update recensioni set id = ?, utente = ?, prodotto = ?, data_acquisto = ?, stelle = ?, recensione = ? where id = ?");
        statement.setInt(1, element.getId());
        statement.setInt(2, element.getUtente());
        statement.setInt(3, element.getProdotto());
        statement.setDate(4, element.getDataAcquisto());
        statement.setInt(5, element.getStelle());
        statement.setString(6, element.getRecensione());
        statement.setInt(7, element.getId());

        statement.executeUpdate();
        statement.close();
        conn.close();
    }

    @Override
    public List<RecensioneBean> doRetrieveOnCondition(Condition<RecensioneBean> cond) throws SQLException {
        List<RecensioneBean> recensioni = new ArrayList<>();
        Connection conn = DBConnector.getConnection();

        Statement statement = conn.createStatement();
        ResultSet set = statement.executeQuery("select * from recensioni");

        while(set.next()) {
            int id = set.getInt("id");
            int utente = set.getInt("utente");
            int prodotto = set.getInt("prodotto");
            Date dataAcquisto = set.getDate("data_acquisto");
            int stelle = set.getInt("stelle");
            String recensione = set.getString("recensione");
            RecensioneBean recensioneBean = new RecensioneBean(id, utente, prodotto, dataAcquisto, stelle, recensione);
            if(cond.test(recensioneBean))
                recensioni.add(recensioneBean);
        }
        statement.close();
        conn.close();
        return recensioni;
    }

    @Override
    public List<RecensioneBean> doRetrieveAll() throws SQLException {
        List<RecensioneBean> recensioni = new ArrayList<>();
        Connection conn = DBConnector.getConnection();

        Statement statement = conn.createStatement();
        ResultSet set = statement.executeQuery("select * from recensioni");

        while(set.next()) {
            int id = set.getInt("id");
            System.out.println(id);
            int utente = set.getInt("utente");
            System.out.println(utente);
            int prodotto = set.getInt("prodotto");
            System.out.println(prodotto);
            Date dataAcquisto = set.getDate("data_acquisto");
            System.out.println(dataAcquisto);
            int stelle = set.getInt("stelle");
            System.out.println(stelle);
            String recensione = set.getString("recensione");
            System.out.println(recensione);
            recensioni.add( new RecensioneBean(id, utente, prodotto, dataAcquisto, stelle, recensione));
        }
        statement.close();
        conn.close();
        return recensioni;
    }

}
