package com.example.apicoltura_mieldoro.model.recensione;

import java.sql.Date;

public class RecensioneBean {
    private int id;
    private int utente;
    private int prodotto;
    private Date dataAcquisto;
    private int stelle;
    private String recensione;

    public RecensioneBean() {}

    public RecensioneBean(int utente, int prodotto, Date dataAcquisto, int stelle, String recensione) {
        this.utente = utente;
        this.prodotto = prodotto;
        this.dataAcquisto = dataAcquisto;
        this.stelle = stelle;
        this.recensione = recensione;
    }

    public RecensioneBean(int id, int utente, int prodotto, Date dataAcquisto, int stelle, String recensione) {
        this.id = id;
        this.utente = utente;
        this.prodotto = prodotto;
        this.dataAcquisto = dataAcquisto;
        this.stelle = stelle;
        this.recensione = recensione;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUtente() {
        return utente;
    }

    public void setUtente(int utente) {
        this.utente = utente;
    }

    public int getProdotto() {
        return prodotto;
    }

    public void setProdotto(int prodotto) {
        this.prodotto = prodotto;
    }

    public Date getDataAcquisto() {
        return dataAcquisto;
    }

    public void setDataAcquisto(Date dataAcquisto) {
        this.dataAcquisto = dataAcquisto;
    }

    public int getStelle() {
        return stelle;
    }

    public void setStelle(int stelle) {
        this.stelle = stelle;
    }

    public String getRecensione() {
        return recensione;
    }

    public void setRecensione(String recensione) {
        this.recensione = recensione;
    }


    public String stringify() {
        String json = "{";
        json += "\"id\": " + this.id + ", ";
        json += "\"utente\": " + this.utente + ", ";
        json += "\"prodotto\": " + this.prodotto + ", ";
        json += "\"dataAcquisto\": \"" + this.dataAcquisto + "\", ";
        json += "\"stelle\": " + this.stelle + ", ";
        json += "\"recensione\": \"" + this.recensione + "\"}";

        return json;
    }
}
