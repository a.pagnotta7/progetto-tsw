package com.example.apicoltura_mieldoro.model.prodotto;

public class ProdottoBean {

    private int id;
    private int tipo;
    private String nome;
    private double costo;
    private int sconto;
    private int quantita;
    private String descrizione;
    private String immagine;
    private int numeroOrdini;


    public ProdottoBean(int id, int tipo, String nome, double costo, int sconto, int quantita, String descrizione, String immagine, int numeroOrdini) {
        this.id = id;
        this.tipo = tipo;
        this.nome = nome;
        this.costo = costo;
        this.sconto = sconto;
        this.quantita = quantita;
        this.descrizione = descrizione;
        this.immagine = immagine;
        this.numeroOrdini = numeroOrdini;
    }

    public ProdottoBean(int tipo, String nome, double costo, int sconto, int quantita, String descrizione, String immagine, int numeroOrdini) {
        this.tipo = tipo;
        this.nome = nome;
        this.costo = costo;
        this.sconto = sconto;
        this.quantita = quantita;
        this.descrizione = descrizione;
        this.immagine = immagine;
        this.numeroOrdini = numeroOrdini;
    }


    public ProdottoBean() {
    }

    public String stringify() {
        String json = "{";
        json += "\"id\": " + this.id + ", ";
        json += "\"tipo\": " + this.tipo + ", ";
        json += "\"nome\": \"" + this.nome + "\", ";
        json += "\"costo\": " + this.costo + ", ";
        json += "\"sconto\": " + this.sconto + ", ";
        json += "\"quantita\": " + this.quantita + ", ";
        json += "\"descrizione\": \"" + this.descrizione + "\", ";
        json += "\"immagine\": \"" + this.immagine + "\", ";
        json += "\"numeroOrdini\": " + this.numeroOrdini + "}";

        return json;
    }

    public int getId() {
        return id;
    }

    public int getTipo() {
        return tipo;
    }

    public String getNome() {
        return nome;
    }

    public double getCosto() {
        return costo;
    }

    public int getSconto() {
        return sconto;
    }

    public int getQuantita() {
        return quantita;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public String getImmagine() {
        return immagine;
    }

    public int getNumeroOrdini() {
        return numeroOrdini;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setCosto(double costo) {
        this.costo = costo;
    }

    public void setSconto(int sconto) {
        this.sconto = sconto;
    }

    public void setQuantita(int quantita) {
        this.quantita = quantita;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public void setImmagine(String immagine) {
        this.immagine = immagine;
    }

    public void setNumeroOrdini(int numeroOrdini) {
        this.numeroOrdini = numeroOrdini;
    }

    @Override
    public String toString() {
        return "ProdottoBean{" +
                "id=" + id +
                ", tipo=" + tipo +
                ", nome='" + nome + '\'' +
                ", costo=" + costo +
                ", sconto=" + sconto +
                ", quantita=" + quantita +
                ", descrizione='" + descrizione + '\'' +
                ", immagine='" + immagine + '\'' +
                ", numeroOrdini=" + numeroOrdini +
                '}';
    }
}

