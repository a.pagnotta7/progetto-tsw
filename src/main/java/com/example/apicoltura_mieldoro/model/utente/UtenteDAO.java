package com.example.apicoltura_mieldoro.model.utente;

import com.example.apicoltura_mieldoro.model.utility.Condition;
import com.example.apicoltura_mieldoro.model.utility.DAO;
import com.example.apicoltura_mieldoro.model.utility.DBConnector;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UtenteDAO implements DAO<UtenteBean> {

    public UtenteDAO() {}

    @Override
    public void doSave(UtenteBean element) throws SQLException {
        Connection conn = DBConnector.getConnection();
        if (element.getId() == -1) {
            PreparedStatement statement = conn.prepareStatement("insert into utente (email,password_,nome,cognome,admin) values (?, ?, ?, ?, ?)");
            statement.setString(1, element.getEmail());
            statement.setString(2, element.getPassword());
            statement.setString(3, element.getNome());
            statement.setString(4, element.getCognome());
            statement.setBoolean(5,element.isAdmin());
            statement.executeUpdate();

            statement.close();
        }
        else{
            PreparedStatement statement = conn.prepareStatement("insert into utente values (?, ?, ?, ?, ?, ?)");
            statement.setInt(1, element.getId());
            statement.setString(2, element.getEmail());
            statement.setString(3, element.getPassword());
            statement.setString(4, element.getNome());
            statement.setString(5, element.getCognome());
            statement.setBoolean(6,element.isAdmin());
            statement.executeUpdate();

            statement.close();
        }

        conn.close();
    }

    @Override
    public void doDelete(UtenteBean element) throws SQLException {
        Connection conn = DBConnector.getConnection();

        PreparedStatement statement = conn.prepareStatement("delete from utente where id = ?");
        statement.setInt(1, element.getId());

        statement.executeUpdate();

        statement.close();
        conn.close();
    }

    @Override
    public void doUpdate(UtenteBean element) throws SQLException {
        Connection conn = DBConnector.getConnection();

        PreparedStatement statement = conn.prepareStatement("update utente set id= ?, email = ?, password_ = ?, nome = ?, cognome = ?, admin = ? where id= ?");
        statement.setInt(1 , element.getId());
        statement.setString(2,element.getEmail());
        statement.setString(3, element.getPassword());
        statement.setString(4, element.getNome());
        statement.setString(5, element.getCognome());
        statement.setBoolean(6,element.isAdmin());
        statement.setInt(7, element.getId());
        statement.executeUpdate();

        statement.close();
        conn.close();
    }

    @Override
    public List<UtenteBean> doRetrieveOnCondition(Condition<UtenteBean> cond) throws SQLException {
        List<UtenteBean> clienti = new ArrayList<>();
        Connection conn = DBConnector.getConnection();

        Statement statement = conn.createStatement();
        ResultSet set = statement.executeQuery("select * from utente");

        while(set.next()) {
            int id = set.getInt("id");
            String email = set.getString("email");
            String password = set.getString("password_");
            String nome = set.getString("nome");
            String cognome = set.getString("cognome");
            boolean admin = set.getBoolean("admin");
            UtenteBean cliente = new UtenteBean(id,email, password, nome, cognome,admin);

            if(cond.test(cliente))
                clienti.add(cliente);
        }

        statement.close();
        conn.close();
        return clienti;
    }

    @Override
    public List<UtenteBean> doRetrieveAll() throws SQLException {
        List<UtenteBean> clienti = new ArrayList<>();
        Connection conn = DBConnector.getConnection();

        Statement statement = conn.createStatement();
        ResultSet set = statement.executeQuery("select * from utente");

        while(set.next()) {
            int id = set.getInt("id");
            String email = set.getString("email");
            String password = set.getString("password_");
            String nome = set.getString("nome");
            String cognome = set.getString("cognome");
            boolean admin = set.getBoolean("admin");
            clienti.add(new UtenteBean(id,email, password, nome, cognome,admin));
        }

        statement.close();
        conn.close();
        return clienti;
    }
}

