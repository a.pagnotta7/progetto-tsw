package com.example.apicoltura_mieldoro.model.ordine;

import com.example.apicoltura_mieldoro.model.utility.Condition;
import com.example.apicoltura_mieldoro.model.utility.DAO;
import com.example.apicoltura_mieldoro.model.utility.DBConnector;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OrdineDAO implements DAO<OrdineBean> {

    public OrdineDAO(){}

    @Override
    public void doSave(OrdineBean element) throws SQLException {
        Connection conn = DBConnector.getConnection();

        PreparedStatement statement = conn.prepareStatement("insert into ordine(data_acquisto, prezzo_totale, spedizione_rapida, utente, nome_ricevente, email_ricevente, indirizzo, citta, cap) values (?, ?, ?, ?, ?, ?, ?, ?, ?)");
        statement.setDate(1, element.getDataAcquisto());
        statement.setDouble(2, element.getPrezzoTotale());
        statement.setBoolean(3, element.isSpedizioneRapida());
        statement.setInt(4, element.getCliente());
        statement.setString(5,element.getNomeRicevente());
        statement.setString(6,element.getEmailRicevente());
        statement.setString(7,element.getIndirizzo());
        statement.setString(8,element.getCitta());
        statement.setString(9, element.getCAP());

        statement.executeUpdate();

        statement.close();
        conn.close();
    }

    @Override
    public void doDelete(OrdineBean element) throws SQLException {
        Connection conn = DBConnector.getConnection();

        PreparedStatement statement = conn.prepareStatement("delete from ordine where id = ?");
        statement.setInt(1, element.getId());

        statement.executeUpdate();

        statement.close();
        conn.close();
    }

    @Override
    public void doUpdate(OrdineBean element) throws SQLException {
        Connection conn = DBConnector.getConnection();

        PreparedStatement statement = conn.prepareStatement("update ordine set id = ?, data_acquisto = ?, prezzo_totale = ?, spedizione_rapida = ?, utente = ?, nome_ricevente = ?, email_ricevente =?, indirizzo=?, citta=?, cap=? where id = ?");
        statement.setInt(1, element.getId());
        statement.setDate(2, element.getDataAcquisto());
        statement.setDouble(3, element.getPrezzoTotale());
        statement.setBoolean(4, element.isSpedizioneRapida());
        statement.setInt(5, element.getCliente());
        statement.setString(6, element.getNomeRicevente());
        statement.setString(7,element.getEmailRicevente());
        statement.setString(8,element.getIndirizzo());
        statement.setString(9,element.getCAP());
        statement.setInt(10,element.getId());

        statement.executeUpdate();

        statement.close();
        conn.close();
    }

    @Override
    public List<OrdineBean> doRetrieveOnCondition(Condition<OrdineBean> cond) throws SQLException {
        List<OrdineBean> ordini = new ArrayList<>();
        Connection conn = DBConnector.getConnection();

        Statement statement = conn.createStatement();
        ResultSet set = statement.executeQuery("select * from ordine");

        while(set.next()) {
            int id = set.getInt("id");
            Date dataAcquisto = set.getDate("data_acquisto");
            double prezzoTotale = set.getDouble("prezzo_totale");
            boolean spedizioneRapida = set.getBoolean("spedizione_rapida");
            int cliente = set.getInt("utente");
            String nomeIndirizzo = set.getString("nome_ricevente");
            String emailIndirizzo = set.getString("email_ricevente");
            String indirizzo = set.getString("indirizzo");
            String cittaIndirizzo = set.getString("citta");
            String CAPIndirizzo = set.getString("cap");
            OrdineBean ordine = new OrdineBean(id, dataAcquisto, prezzoTotale, spedizioneRapida, cliente, nomeIndirizzo,emailIndirizzo,indirizzo,cittaIndirizzo,CAPIndirizzo);

            if(cond.test(ordine))
                ordini.add(ordine);
        }

        statement.close();
        conn.close();
        return ordini;
    }

    @Override
    public List<OrdineBean> doRetrieveAll() throws SQLException {
        List<OrdineBean> ordini = new ArrayList<>();
        Connection conn = DBConnector.getConnection();

        Statement statement = conn.createStatement();
        ResultSet set = statement.executeQuery("select * from ordine");

        while(set.next()) {
            int id = set.getInt("id");
            Date dataAcquisto = set.getDate("data_acquisto");
            double prezzoTotale = set.getDouble("prezzo_totale");
            boolean spedizioneRapida = set.getBoolean("spedizione_rapida");
            int cliente = set.getInt("utente");
            String nomeIndirizzo = set.getString("nome_ricevente");
            String emailIndirizzo = set.getString("email_ricevente");
            String indirizzo = set.getString("indirizzo");
            String cittaIndirizzo = set.getString("citta");
            String CAPIndirizzo = set.getString("cap");
            OrdineBean ordine = new OrdineBean(id, dataAcquisto, prezzoTotale, spedizioneRapida, cliente, nomeIndirizzo,emailIndirizzo,indirizzo,cittaIndirizzo,CAPIndirizzo);
        }

        statement.close();
        conn.close();
        return ordini;
    }

    public int getLastInsertID() throws SQLException {
        Connection conn = DBConnector.getConnection();
        Statement statement = conn.createStatement();
        ResultSet set = statement.executeQuery("SELECT LAST_INSERT_ID(id) from ordine order by LAST_INSERT_ID(id) desc limit 1;");

        if(set.next())
            return set.getInt("LAST_INSERT_ID(id)");

        return -1;
    }

}
