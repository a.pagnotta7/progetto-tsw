package com.example.apicoltura_mieldoro.model.ordine;

import java.sql.Date;

public class OrdineBean {
    private int id;
    private Date dataAcquisto;
    private double prezzoTotale;
    private boolean spedizioneRapida;
    private int cliente;

    private String nomeRicevente;
    private String emailRicevente;
    private String indirizzo;
    private String citta;
    private String CAP;

    public OrdineBean(){}

    public OrdineBean(Date dataAcquisto, double prezzoTotale, boolean spedizioneRapida, int cliente, String nomeRicevente, String emailRicevente, String indirizzo, String citta, String CAP) {
        this.dataAcquisto = dataAcquisto;
        this.prezzoTotale = prezzoTotale;
        this.spedizioneRapida = spedizioneRapida;
        this.cliente = cliente;
        this.nomeRicevente = nomeRicevente;
        this.emailRicevente = emailRicevente;
        this.indirizzo = indirizzo;
        this.citta = citta;
        this.CAP = CAP;
    }

    public OrdineBean(int id, Date dataAcquisto, double prezzoTotale, boolean spedizioneRapida, int cliente, String nomeRicevente, String emailRicevente, String indirizzo, String citta, String CAP) {
        this.id = id;
        this.dataAcquisto = dataAcquisto;
        this.prezzoTotale = prezzoTotale;
        this.spedizioneRapida = spedizioneRapida;
        this.cliente = cliente;
        this.nomeRicevente = nomeRicevente;
        this.emailRicevente = emailRicevente;
        this.indirizzo = indirizzo;
        this.citta = citta;
        this.CAP = CAP;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDataAcquisto() {
        return dataAcquisto;
    }

    public void setDataAcquisto(Date dataAcquisto) {
        this.dataAcquisto = dataAcquisto;
    }

    public double getPrezzoTotale() {
        return prezzoTotale;
    }

    public void setPrezzoTotale(double prezzoTotale) {
        this.prezzoTotale = prezzoTotale;
    }

    public boolean isSpedizioneRapida() {
        return spedizioneRapida;
    }

    public void setSpedizioneRapida(boolean spedizioneRapida) {
        this.spedizioneRapida = spedizioneRapida;
    }

    public int getCliente() {
        return cliente;
    }

    public void setCliente(int cliente) {
        this.cliente = cliente;
    }

    public String getNomeRicevente() {
        return nomeRicevente;
    }

    public void setNomeRicevente(String nomeIndirizzo) {
        this.nomeRicevente = nomeIndirizzo;
    }

    public String getEmailRicevente() {
        return emailRicevente;
    }

    public void setEmailRicevente(String emailIndirizzo) {
        this.emailRicevente = emailIndirizzo;
    }

    public String getIndirizzo() {
        return indirizzo;
    }

    public void setIndirizzo(String indirizzo) {
        this.indirizzo = indirizzo;
    }

    public String getCitta() {
        return citta;
    }

    public void setCitta(String cittaIndirizzo) {
        this.citta = cittaIndirizzo;
    }

    public String getCAP() {
        return CAP;
    }

    public void setCAP(String CAPIndirizzo) {
        this.CAP = CAPIndirizzo;
    }
}

