<%@ page import="com.example.apicoltura_mieldoro.model.prodotto.ProdottoBean" %><%--
  Created by IntelliJ IDEA.
  User: antonio
  Date: 22/07/23
  Time: 11:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    ProdottoBean p = (ProdottoBean) request.getAttribute("prodottoBean");
    String nome = p.getNome();
    String descrizione = p.getDescrizione();
    String immagine = p.getImmagine();
    Double costo = p.getCosto();
    int sconto = p.getSconto();
    int quantitaRimasta = p.getQuantita();
    int tipo = p.getTipo();
    int numeroOrdini = p.getNumeroOrdini();
    int id = p.getId();
%>
<html>
<head>

    <title>Apicoltura Mieldoro - AGGIORNA PRODOTTO</title>
    <link rel="stylesheet" type="text/css" href="css/editPage.css">
</head>
<body>
<%@include file="WEB-INF/includes/header.jsp"%>
<h2>Modifica il Prodotto:</h2>
<form method="post" action="ProductEdit" class="container" enctype='multipart/form-data'>
    <div class="rowContainer">
        <label for="immagine">Immagine:</label>
        <input type="file" name="immagine" id="immagine" value="<%=immagine%>">
        <label for="tipo">Tipo: </label>
        <select name="tipo" id="tipo">
            <%
                String miele ="";
                String attrezzature ="";
                String accessori ="";
                if(tipo == 1){
                    miele ="selected";
                } else if (tipo ==2) {
                    attrezzature ="selected";
                } else if (tipo == 3) {
                    accessori ="selected";
                }
            %>
            <option value="1" <%=miele%>>Pesce</option>
            <option value="2" <%=attrezzature%>>Molluschi</option>
            <option value="3" <%=accessori%>>Crostacei</option>
        </select>

        <label for="nome">Nome: </label>
        <input type="text" name="nome" id="nome" value="<%=nome%>">
        <label for="descrizione">Descrizione: </label>
        <textarea name="descrizione" id="descrizione" cols="30" rows="10"><%=descrizione%></textarea>
        <label for="costo">Costo: </label>
        <input type="number" step="0.01" id="costo" name="costo" value="<%=costo%>">
        <label for="sconto">Sconto: </label>
        <input type="number" id="sconto" name="sconto" max="100" min="0" value="<%=sconto%>">
        <label for="quantita">Quantit&agrave; Rimasta:</label>
        <input type="number" id="quantita" name="quantita" min="0" value="<%=quantitaRimasta%>">
        <input type="hidden" id="numeroOrdini" name="numeroOrdini" value="<%=numeroOrdini%>">
        <input type="hidden" id="pathOriginale" name="pathOriginale" value="<%=immagine%>">
        <input type="hidden" id="id" name="id" value="<%=id%>">
        <input type="hidden" name="edit" value="true">
        <input type="submit" value="Aggiorna Prodotto">
    </div>
</form>
<%@include file="WEB-INF/includes/footer.html"%>
</body>
</html>

