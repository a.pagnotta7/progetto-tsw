<%--
  Created by IntelliJ IDEA.
  User: antonio
  Date: 06/07/23
  Time: 20:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="stylesheet/header.css?<?php echo time(); ?>">
    <link rel="stylesheet" href="stylesheet/footer.css?<?php echo time(); ?>">
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="icon" type="image/x-icon" href="images/favicon-32x32.png">
    <link rel="stylesheet" href="stylesheet/chisiamo.css">
    <title>Apicoltura Mieldoro - Chi siamo</title>

</head>
<body style="width: 100%; height: 100%">
    <%@include file="WEB-INF/includes/header.jsp"%><br>
    <div class="slideshow-container" style="width: 100%">
        <div class="mySlides fade">
            <img src="images/2022-03-27.jpg" style="width:100%; height:100%;">
        </div>

        <div class="mySlides fade">
            <img src="images/paradise.jpg" style="width:100%; height:100%;">
        </div>

        <div class="mySlides fade">
            <img src="images/IMG-20190316-WA0019.jpg" style="width:100%; height:100%;">
        </div>
        <div class="centered"><h2>Apicoltura Mieldoro</h2></div>
    </div>
    <p class="descr-container"><br><br> L’azienda Apicoltura Mieldoro é nata per la passione di un Perito Agrario,
        appassionato del mondo agrario fin da ragazzino, il quale ha riscoperto le
        tradizioni legate al suo territorio tra cui anche “Le Api”
        (produttrici indiscusse di mieli prelibati, ). Il miele ha da sempre riempito
        la tavola dei nostri avi, e ancor oggi continuano a deliziare i palati dei
        buongustai e degli appassionati culinari. L’azienda è situata a 8 km dal capoluogo
        ed a 517 m di altezza sulle verdi ed incontaminate colline di San Potito Ultra,
        in un piccolo comune della provincia di Avellino lontano da centri urbani e da
        ogni fonte di inquinamento, una zona di grandi bellezze naturali, a forte vocazione
        rurale, ma con attività artigianali di grande originalità. Da Apicoltura Mieldoro
        troverete miele, polline, pappa reale, cera e propoli. Prodotti naturali ricavati
        dalla passione e dalla professionalità di questa azienda che vi offre un prodotto
        di altissima qualità. L’azienda a conduzione
        familiare guidata da Stanislao Pagnotta, punta ad un prodotto di alta qualità e si
        occupa principalmente dell’allevamento di api e quindi dedita alla produzione di
        miele, pappa reale, cera, polline, propoli, celle reali e api regine. L’azienda è
        provvista di un centro didattico per l’osservazione delle api adatto a scolaresche,
        famiglie e bambini, quindi si effettuano, su prenotazione, visite guidate con la
        possibilità di visitare gli alveari avvicinando così tutti al fantastico, magico e
        laborioso monde delle api.
    </p><br><br>
    <section class="about">
        <div class="about-us">
            <div class="image container">
                <img src="images/stanymieldoro.jpg" alt="About Us" class="image2">
                <div class="overlay">Stanislao Pagnotta</div>
            </div>
            <div class="about-txt">
                <h2>Stanislao Pagnotta</h2>
                <p>L’azienda a conduzione familiare guidata da Stanislao Pagnotta, punta ad un prodotto
                    di alta qualità e si occupa principalmente dell’allevamento di api e quindi dedita
                    alla produzione di miele, pappa reale, cera, polline, propoli, celle reali e api
                    regine. L’azienda è provvista di un centro didattico per l’osservazione delle api
                    adatto a scolaresche,
                    famiglie e bambini, quindi si effettuano, su prenotazione, visite guidate con la
                    possibilità di visitare gli alveari avvicinando così tutti al fantastico, magico e
                    laborioso monde delle api.
                </p>
            </div>
        </div>
    </section>
    <div>
        <div  class="map-container">
            <div class="map about-txt">
                <h2>Vieni a trovarci</h2>
                <h3>-Via Breccelle Rosse 16</h3><br>
                <iframe class="gmaps" width="100%" height="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=en&amp;q=Contrada%20Breccelle%20rosse,%206a,%2083050%20San%20Potito%20ultra%20AV+(Apicoltura%20Mieldoro)&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"><a href="https://www.maps.ie/distance-area-calculator.html">distance maps</a></iframe>
            </div>
        </div>
    </div>


    <script>
        let slideIndex = 0;
        showSlides();

        function showSlides() {
            let i;
            let slides = document.getElementsByClassName("mySlides");
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }
            slideIndex++;
            if (slideIndex > slides.length) {slideIndex = 1}
            slides[slideIndex-1].style.display = "block";
            setTimeout(showSlides, 5000);
        }
    </script>
    <%@include file="WEB-INF/includes/footer.html"%><br>
</body>
</html>
