<%@ page import="com.example.apicoltura_mieldoro.model.prodotto.ProdottoBean" %>
<%@ page import="com.example.apicoltura_mieldoro.model.carrello.CarrelloBean" %>
<%@ page import="com.example.apicoltura_mieldoro.model.carrello.CartEntry" %>
<%@ page import="java.util.List" %>
<%
    ProdottoBean p = (ProdottoBean) request.getAttribute("prodottoBean");
    int id = p.getId();
    String nome = p.getNome();
    String descrizione = p.getDescrizione();
    String immagine = p.getImmagine();
    Double costo = p.getCosto();
    int sconto = p.getSconto();
    int quantita = p.getQuantita();
    int tipo = p.getTipo();
    boolean isOnDiscount = sconto != 0;
    double costoReale = costo*(100-sconto)/100;
    List<CartEntry> cartL;
    cartL = (List<CartEntry>) session.getAttribute("carrello");
    System.out.println("\n\n\n\n" + cartL + "\n\n\n\n");
    CarrelloBean prod = null;
    double cartPrice = 0;
    if (cartL != null && !cartL.isEmpty()) {
        System.out.println("\n\n\n\n" + "nell' if" + "\n\n\n\n");
        for (CartEntry entry : cartL) {
            if(entry.getEntry().getProdotto() == id){
                prod = entry.getEntry();
            }
        }
    }
    String block = "none";
    if(prod != null) {
        int quan = prod.getQuantita();
        System.out.println("\n\n\n\n quantita= " + quantita + " quant= " + quan + "\n\n\n\n");

        if (quan != 0) {
            if ((quantita - quan) > 0) {
                block = "none";
            }else {
                block = "block";
            }
        } else {
            block = "none";
        }
    }

%>
<%--
  Created by IntelliJ IDEA.
  User: antonio
  Date: 06/07/23
  Time: 20:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="stylesheet/header.css?<?php echo time(); ?>">
        <link rel="stylesheet" href="stylesheet/footer.css?<?php echo time(); ?>">
        <link rel="stylesheet" href="stylesheet/productPage.css?<?php echo time(); ?>">
        <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Pompiere:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="icon" type="image/x-icon" href="images/favicon-32x32.png">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
        <script src="scripts/cards.js"></script>
        <script src="scripts/cart.js"></script>
        <script src="scripts/reviews.js"></script>
        <script src="scripts/slidingCardsProductPage.js"></script>
        <title>Apicoltura Mieldoro - <%= nome %></title>
    </head>
        <body>
        <%@include file="WEB-INF/includes/header.jsp"%><br>
        <h2 class="divider productLabel" style="visibility: visible"> Prodotto</h2>
        <div class="container">
            <div class="product">
                <div class="gallery">
                    <img src="<%= immagine %>" id="productImg" alt="">
                </div>
                <div class="details">
                    <h1><%= nome %></h1>
                    <h2><%= costoReale %> €</h2>
                    <% if(sconto > 0){%>
                    <h3><%= sconto %> %</h3>
                    <% } %>
                    <p><%= descrizione %></p>
                    <div id="carrelloForm">
                        <div class="quantity-select">
                            <p>Quantità</p>
                            <select name="quantity" form="carrelloForm" id="quantitySelector" value="1">
                                <%
                                for(int i = 1; i <= quantita; i++){
                                    if(i == 1){
                                %>
                                <option value="<%= i %> selected"><%= i %></option>
                                <%
                                }else{
                                %>
                                <option value="<%= i %> "><%= i %></option>
                                <%
                                    }
                                }
                                %>
                            </select>
                        </div>
                        <button class="GFG" id="button<%= id %>-big" onclick="addToCart(<%= id %>, 1); changeLabel(<%= id %>, true)">Aggiungi al carrello </button>
                        <p id="errorOutcome" style="display: <%=block%>"> Max disponibile nel carrello.</p>
                    </div>
                </div>
            </div>
        </div>
        <div id="reviewsContainer">
            <div class="reviewsDiv" id="reviewsDiv">
            </div>
        </div>

        <div class="topProducts">
            <h2 class="divider"> Consigliati</h2>
            <div class="cardContainer" id="topProdotti"></div>
        </div>
        <br>

        <!-- The dots/circles -->
        <div style="text-align:center">
            <span class="dot" id="dot1" onclick="currentDiv(1)"></span>
            <span class="dot" id="dot2" onclick="currentDiv(2)"></span>
            <span class="dot" id="dot3" onclick="currentDiv(3)"></span>
            <span class="dot" id="dot4" onclick="currentDiv(4)"></span>
            <span class="dot" id="dot5" onclick="currentDiv(5)"></span>
            <span class="dot" id="dot6" onclick="currentDiv(6)"></span>
        </div>
        <br><br><br><br><br><br><br><br><br><br><br><br><br>
        <script>
            const xhttp = new XMLHttpRequest();


            function productsInit(){
                setCardType(0);
                setElementID("topProdotti");
                $.ajax({
                    url:"ProGetter?type=<%= tipo %>&top=6",
                    success: function(result){
                        createCards(result);
                        currentDiv(1);
                    }
                })
            }
            productsInit();
            reviewsGetter(<%= id %>);

            function reviewsGetter(id) {
                setElementID1("reviewsDiv");
                $.post({
                    url:"ReviewsServlet?prodottoRec=" + id,
                    success: function(result){
                        createReviews(result);
                    }
                })
            }

        </script>
        <%@ include file="WEB-INF/includes/footer.html"%>
    </body>
</html>
