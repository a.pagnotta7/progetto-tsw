<%@ page import="com.example.apicoltura_mieldoro.model.utente.UtenteBean" %><%--
  Created by IntelliJ IDEA.
  User: antonio
  Date: 22/07/23
  Time: 11:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="stylesheet/header.css?<?php echo time(); ?>">
  <link rel="stylesheet" href="stylesheet/pageCreator.css?<?php echo time(); ?>">
  <link rel="stylesheet" href="stylesheet/footer.css?<?php echo time(); ?>">
  <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="icon" type="image/x-icon" href="images/favicon-32x32.png">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
  <title>Apicoltura Mieldoro - Aggiunta prodotto</title>
</head>
<body>
<%if(session.getAttribute("isLoggedIn") != null){
    System.out.println("diverso da null");
    boolean isLoggedIn = Boolean.parseBoolean((String) session.getAttribute("isLoggedIn"));
    if(isLoggedIn == false){
      System.out.println("falso");
      response.sendRedirect("index.jsp");
    }
  }else{
      System.out.println("true");
  response.sendRedirect("index.jsp");
  }
%>
<%@include file="WEB-INF/includes/header.jsp"%>
<h2 id="addProductLabel">Aggiungi prodotto:</h2>
<form method="post" action="ProductEdit?edit=false" class="container" enctype='multipart/form-data'>
  <div class="rowContainer">
    <label for="tipo">Tipo: </label>
    <select name="tipo" id="tipo">
      <option value="1">Miele</option>
      <option value="2">Attrezzature</option>
      <option value="3">Accessori</option>
    </select>
    <label for="nome">Nome: </label>
    <input type="text" name="nome" id="nome">
    <label for="costo">Costo: </label>
    <input type="number" step="0.01" id="costo" name="costo">
    <label for="sconto">Sconto: </label>
    <input type="number" id="sconto" name="sconto" max="100" min="0">
    <label for="quantita">Quantit&agrave; Rimasta:</label>
    <input type="number" id="quantita" name="quantita" min="0">
    <label for="descrizione">Descrizione: </label>
    <textarea name="descrizione" id="descrizione" cols="30" rows="10"></textarea>
    <label for="immagine">Immagine:</label>
    <input type="file" name="immagine" id="immagine">
    <input type="submit" value="Inserisci Prodotto">
  </div>



</form>
<%@include file="WEB-INF/includes/footer.html"%>
<script>
  function redirect() {
    window.location.replace("index.jsp");
  }
</script>
</body>
</html>

