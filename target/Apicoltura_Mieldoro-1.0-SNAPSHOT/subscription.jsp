<!DOCTYPE html>
<html>
<head>
    <link rel="icon" type="image/x-icon" href="images/favicon-32x32.png">
    <link rel="stylesheet" href="stylesheet/subscription.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script type="text/javascript" src="scripts/validateForm.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Apicoltura Mieldoro - Iscrizione</title>
</head>
<body>
<form name="form" action="AuthenticationServlet" onsubmit="return validateSubForm()" method="post">
    <div class="title">Benvenuto</div>
    <div class="subtitle">Creazione Account</div>
    <%
        String block = "none";
        String outcome = (String) request.getAttribute("outcome");
        if(outcome != null) {
            if (outcome.equals("false"))
                block = "block";
            if (outcome.equals("true"))
                block = "none";
        }else if(outcome == null)
            block = "none";
    %>
    <div id="error" class="error"  style="display: <%=block%>">Riprova!</div>
    <input type="hidden" name="type" value="register">
    <div class="input-container ic1">
        <input id="firstname" name="firstname" class="input" type="text" placeholder="Nome" />
        <div class="cut"></div>
    </div>
    <div class="input-container ic2">
        <input id="lastname" name="lastname" class="input" type="text" placeholder="Cognome" />
        <div class="cut"></div>
    </div>
    <div class="input-container ic2">
        <input id="email" name="email" class="input" type="email" placeholder="Email" />
        <div class="cut cut-short"></div>
    </div>
    <div class="input-container ic2">
        <input id="password" name="password" class="input" type="password" placeholder="Password" />
        <div class="cut cut-short"></div>
    </div>
    <div class="login">Hai gi&agrave; un account? <a href="login.jsp">Login</a></div>
    <button type="text" class="submit">Invia</button>
</form>
</body>
</html>

