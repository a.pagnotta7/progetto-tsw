<script>
    let open = 0;
    function dropMenu() {
        let width = screen.width;

        if(width <= 925){
            return;
        }
        if(open == 0){
            open = 1;
            document.getElementById('drop-menu').style.visibility = "visible";
            document.getElementById('menu-cart2').style.display = "block";
            document.getElementById('menu-products2').style.display = "block";
            document.getElementById('menu-aboutUs2').style.display = "block";
            document.getElementById('menu-contactUs2').style.display = "block";
            document.getElementById('menu-log2').style.display = "block";
        }else{
            open = 0;
            document.getElementById('drop-menu').style.visibility = "hidden";
            document.getElementById('menu-cart2').style.display = "none";
            document.getElementById('menu-products2').style.display = "none";
            document.getElementById('menu-aboutUs2').style.display = "none";
            document.getElementById('menu-contactUs2').style.display = "none";
            document.getElementById('menu-log2').style.display = "none";
        }
    }

    window.addEventListener('resize', function(event) {
        let width = screen.width;
        if(width > 915){
            document.getElementById('drop-menu').style.visibility = "hidden";
            document.getElementById('menu-cart2').style.display = "none";
            document.getElementById('menu-products2').style.display = "none";
            document.getElementById('menu-aboutUs2').style.display = "none";
            document.getElementById('menu-contactUs2').style.display = "none";
            document.getElementById('menu-log2').style.display = "none";
        }
    }, true);

</script>
<header>
    <div class="inner">
        <div class="logo">
            <div>
                <a href="../progetto_tsw_war/"><img src="images/logo.png" alt="Logo"></a>
            </div>
        </div>
        <%
            if(session.getAttribute("isLoggedIn") == null)
            {
                session.setAttribute("isLoggedIn","false");
            }
            boolean isLoggedIn = Boolean.parseBoolean((String) session.getAttribute("isLoggedIn"));
        %>

        <nav id="normal-menu">
            <li id="menu-cart"><a href="carrello.jsp"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a></li>
            <li id="menu-products"><span><a href="products.jsp" style="font-family: 'Pompiere', display">Prodotti</a></span></li>
            <li id="menu-aboutUs"><span><a href="chisiamo.jsp" style="font-family: 'Pompiere', display">Chi siamo</a></span></li>
            <li id="menu-contactUs"><span><a href="mailto:antoniopagnotta335@gmail.com" style="font-family: 'Pompiere', display">Contattaci</a></span></li>
            <%
            if(isLoggedIn){ %>
            <li id="menu-log"><span><a id="login-menu" href="UserPage" class="buttonLogin" style="font-family: 'Pompiere', display">Area Utenti</a></span></li> <%
            }
            else{ %>
            <li id="menu-log"><span><a id="login-menu" href="login.jsp" class="buttonLogin" style="font-family: 'Pompiere', display">Login</a></span></li> <%
            }
            %>
            <li id="menu-drop"><span><input id="menu-drop-img" type="image" src="images/menu.png" onclick="dropMenu()" /></span></li>
        </nav>
    </div>
</header>
<div id="drop-menu">
    <li id="menu-cart2"><a href="carrello.jsp">Carrello</a></li>
    <li id="menu-products2"><span><a href="products.jsp">Prodotti</a></span></li>
    <li id="menu-aboutUs2"><span><a href="chisiamo.jsp">Chi siamo</a></span></li>
    <li id="menu-contactUs2"><span><a href="mailto:antoniopagnotta335@gmail.com">Contattaci</a></span></li><%
    if(isLoggedIn){ %>
    <li id="menu-log2"><span><a id="login-menu2" href="UserPage" class="button">Area Utenti</a></span></li> <%
    }
    else{ %>
    <li id="menu-log2"><span><a id="login-menu2" href="login.jsp" class="button">Login</a></span></li> <%
    }
    %>
</div>
