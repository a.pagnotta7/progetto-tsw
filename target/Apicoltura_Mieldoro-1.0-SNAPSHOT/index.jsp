<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="stylesheet/header.css?<?php echo time(); ?>">
        <link rel="stylesheet" href="stylesheet/index.css?<?php echo time(); ?>">
        <link rel="stylesheet" href="stylesheet/footer.css?<?php echo time(); ?>">
        <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="icon" type="image/x-icon" href="images/favicon-32x32.png">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
        <script src="scripts/cards.js"></script>
        <script src="scripts/slidingCards.js"></script>
        <script src="scripts/cart.js"></script>

        <title>Apicoltura Mieldoro</title>
    </head>
    <body style="width: 100%; height: 100%;">
        <div class="firstPart">
            <%@include file="WEB-INF/includes/header.jsp"%><br>

            <div>
                <div class="mainImageDivide">
                    <img class="mainImage" src="images/index.jpg" style="width:100%">
                </div>
            </div>

            <h2 class="divider1"> Scopri i nostri prodotti</h2>
            <div class="grid-container">
                <div>
                    <img class="productImage" id="pI1" src="images/miele1.png" style="width:100%">
                    <a href="products.jsp?index=1"><h1 id="pIT1" >Miele</h1></a>
                </div>
                <div>
                    <img class="productImage" id="pI2" src="images/attrezzatura1.png" style="width:100%">
                    <a href="products.jsp?index=2"><h1 id="pIT2" >Attrezzatura</h1></a>
                </div>
            </div>

            <br>
            <div class="topProducts">
                <h2 class="divider"> Prodotti del momento</h2> <h3 class="discontProducts"> &#126  Approffitta dei prezzi</h3>
                <div class="cardContainer" id="topProdotti"></div>
            </div>
            <br>

            <!-- The dots/circles -->
            <div style="padding-bottom: 80px; text-align:center">
                <span class="dot" id="dot1" onclick="currentDiv(1)"></span>
                <span class="dot" id="dot2" onclick="currentDiv(2)"></span>
                <span class="dot" id="dot3" onclick="currentDiv(3)"></span>
                <span class="dot" id="dot4" onclick="currentDiv(4)"></span>
                <span class="dot" id="dot5" onclick="currentDiv(5)"></span>
                <span class="dot" id="dot6" onclick="currentDiv(6)"></span>
            </div>
        </div>

        <script>
            const xhttp = new XMLHttpRequest();


            function productsInit(){
                setCardType(0);
                setElementID("topProdotti");
                $.ajax({
                    url:"ProGetter?type=1&top=6",
                    success: function(result){
                        createCards(result);
                        currentDiv(1);
                    }
                })
            }
            productsInit();
        </script>


            <br><br><br><br><br><br><br><br><br><br><br><br><br>
        <%@include file="WEB-INF/includes/footer.html"%><br>
    </body>
</html>