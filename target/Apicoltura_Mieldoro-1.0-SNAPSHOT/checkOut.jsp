<%--
  Created by IntelliJ IDEA.
  User: antonio
  Date: 21/07/23
  Time: 17:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.example.apicoltura_mieldoro.model.utente.*" %>
<%@ page import="com.example.apicoltura_mieldoro.model.carrello.*" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Locale" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="stylesheet/header.css?<?php echo time(); ?>">
    <link rel="stylesheet" href="stylesheet/checkOut.css?<?php echo time(); ?>">
    <link rel="stylesheet" href="stylesheet/footer.css?<?php echo time(); ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
    <link rel="icon" type="image/x-icon" href="images/favicon-32x32.png">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script src="scripts/cards.js"></script>
    <script src="scripts/checkOutValidate.js"></script>
    <title>Apicoltura Mieldoro - Ordine</title>
</head>
<body>
<%@include file="WEB-INF/includes/header.jsp"%>

<%
    int id = 0;
    double totalPrice = 0;
    if(session.getAttribute("isLoggedIn") == null || session.getAttribute("utente") == null){
        String site = new String("index.jsp");
        response.setStatus(response.SC_MOVED_TEMPORARILY);
        response.setHeader("Location", site);
    }else{
    UtenteBean user = (UtenteBean) session.getAttribute("utente");
    if(user == null){
        String site = new String("index.jsp");
        response.setStatus(response.SC_MOVED_TEMPORARILY);
        response.setHeader("Location", site);
    }
    id = user.getId();
    List<CartEntry> cart = (List<CartEntry>) session.getAttribute("carrello");
    for(CartEntry entry: cart) {
        totalPrice += (entry.getProdotto().getCosto() * 100 - (entry.getProdotto().getCosto() * entry.getProdotto().getSconto())) * entry.getEntry().getQuantita();
    }
    }
    if(totalPrice == 0){
        String site = new String("index.jsp");
        response.setStatus(response.SC_MOVED_TEMPORARILY);
        response.setHeader("Location", site);
    }
    %>
<div class="orderContainer">
    <form action="CheckOutValidator" class="formContainer" method="post" onsubmit="return validate()">
        <div class="rowContainer">
            <h2> Indirizzo di Spedizione</h2>
            <input type="hidden" name="userID" value="<%=id%>">
            <label for="nome"><span> Nome</span></label>
            <input type="text" id="nome" name="nome" placeholder="Nome Cognome" required><br>
            <label for="email"><span> Email</span></label>
            <input type="text" id="email" name="email" placeholder="example@email.com" required><br>
            <label for="indirizzo"><span> Indirizzo</span></label>
            <input type="text" id="indirizzo" name="indirizzo" placeholder="Via Vittorio Emanuele" required><br>
            <label for="citta"> <span> Citt&agrave;</span> </label>
            <input type="text" id="citta" name="citta" placeholder="Roma" required><br>
            <label for="CAP"><span>CAP</span></label>
            <input type="text" name="CAP" id="CAP" placeholder="00100" required><br>
        </div>
        <div class="rowContainer">
            <h2>Pagamento</h2>
            <label for="cname"><span> Intestatario</span></label>
            <input type="text" id="cname" name="nomeCarta" placeholder="Nome Cognome" required><br>
            <label for="ccnum"><span> Numero di Carta</span></label>
            <input type="text" id="ccnum" name="numeroCarta" placeholder="0000-0000-0000-0000" required><br>
            <label for="expmonth"><span> Scadenza</span></label>
            <input type="month" id="expmonth" name="meseScadenza" placeholder="Agosto" required><br>
            <label for="cvv"><span> CVV</span></label>
            <input type="text" id="cvv" name="cvv" placeholder="333" required><br>
            <div class="deliveryContainer">
                <label for="delivery"><span> Spedizione Rapida</span></label>
                <input type="checkbox" name="spedizioneRapida" id="delivery" value="true" onchange="setFastDelPrice()">

            </div>
            <span>Totale: <span id="totalPrice"><%=String.format(Locale.US, "%.2f", totalPrice / 100)%></span>&euro;</span>
        </div>

        <input type="submit" value="Ordina" class="orderButton"> <br>
        <span id="errorMsg" style="visibility: hidden; text-align: center"></span>
    </form>
</div>
<script>
    function redirect() {
        window.location.replace("index.jsp");
    }

    function setFastDelPrice() {
        const baseTotal = <%=Math.ceil(totalPrice)%> / 100;
        let totalPrice = parseFloat($("#totalPrice").text());
        let fastDelivery = document.getElementById("delivery").checked;

        if(fastDelivery && totalPrice === baseTotal) {
            totalPrice += 5;
        }
        else if(!fastDelivery && totalPrice > baseTotal) {
            totalPrice -= 5;
        }

        $("#totalPrice").text(totalPrice.toFixed(2));
    }
</script>

<%@include file="WEB-INF/includes/footer.html"%>
</body>
</html>

