let isCarrelloVuoto = 0;

function changeLabel(id, bool) {
    if(!bool) {
        document.getElementById("button" + id).innerText = "Aggiunto al carrello!";
        setTimeout(resetLabel, 500, id);
    }else {
        document.getElementById("button" + id + "-big").innerText = "Aggiunto al carrello!";
        setTimeout(resetLabelBig, 500, id);
    }
}

function resetLabel(id){
    document.getElementById("button" + id).innerText = "Aggiugi al carrello";
}

function resetLabelBig(id){
    document.getElementById("button" + id + "-big").innerText = "Aggiugi al carrello";
}

function addToCart(id, origin) {
    let quantitySelector = "1";
    if(document.getElementById("quantitySelector") == null) {
        quantitySelector = "1";
    }else{
        quantitySelector = document.getElementById("quantitySelector").value;
    }
    var thenum = quantitySelector.match(/\d/g);
    thenum = thenum.join("");
    console.log("quantitySelector = " + thenum );
    if(document.getElementById("quantitySelector") == null){
    $.get("CartHandler?mode=add&id=" + id,
        function (){
            console.log("id = " + id + " origin = " + origin);
            if(parseInt(origin) == 0) {
                console.log("id = " + id + " max = " + parseInt(document.getElementById("maxQuantity" + id).value) + "quantity = " + parseInt(document.getElementById("quantityOf" + id).innerText));
                if (parseInt(document.getElementById("maxQuantity" + id).value) > parseInt(document.getElementById("quantityOf" + id).innerText)) {
                    console.log(parseInt(document.getElementById("quantityOf" + id).innerText));
                    let quantity = parseInt(document.getElementById("quantityOf" + id).innerText) + 1;
                    let singlePrice = parseFloat(document.getElementById("singlePriceOf" + id).innerText);
                    console.log("quantity = " + quantity);
                    console.log("singlePrice = " + singlePrice);
                    document.getElementById("quantityOf" + id).innerText = quantity.toString();
                    document.getElementById("totalPriceOf" + id).innerText = (singlePrice * quantity).toFixed(2);
                    document.getElementById("cartPrice").innerText = (parseFloat(document.getElementById("cartPrice").innerText) + singlePrice).toFixed(2);
                }
            }else{
                console.log("malamente id = " + id + " origin = " + origin);
                location.reload();
            }
        }
    );
    }else{
        $.get("CartHandler?mode=add&id=" + id + "&quantity=" + thenum,
            function (){
                console.log("id = " + id + " origin = " + origin);
                if(parseInt(origin) == 0) {
                    console.log("id = " + id + " max = " + parseInt(document.getElementById("maxQuantity" + id).value) + "quantity = " + parseInt(document.getElementById("quantityOf" + id).innerText));
                    if (parseInt(document.getElementById("maxQuantity" + id).value) > parseInt(document.getElementById("quantityOf" + id).innerText)) {
                        console.log(parseInt(document.getElementById("quantityOf" + id).innerText));
                        let quantity = parseInt(document.getElementById("quantityOf" + id).innerText) + 1;
                        let singlePrice = parseFloat(document.getElementById("singlePriceOf" + id).innerText);
                        console.log("quantity = " + quantity);
                        console.log("singlePrice = " + singlePrice);
                        document.getElementById("quantityOf" + id).innerText = quantity.toString();
                        document.getElementById("totalPriceOf" + id).innerText = (singlePrice * quantity).toFixed(2);
                        document.getElementById("cartPrice").innerText = (parseFloat(document.getElementById("cartPrice").innerText) + singlePrice).toFixed(2);
                    }
                }else{
                    console.log("malamente id = " + id + " origin = " + origin);
                    location.reload();
                }
            }
        );
    }

}

function handler(id, origin) {
    if(origin == 0) {
        if (parseInt(document.getElementById("maxQuantity" + id).value) >= parseInt(document.getElementById("quantityOf" + id).value)) {
            console.log(parseInt(document.getElementById("quantityOf" + id).value));
            let quantity = parseInt(document.getElementById("quantityOf" + id).innerHTML) + 1;
            let singlePrice = parseFloat(document.getElementById("singlePriceOf" + id).innerHTML);
            document.getElementById("quantityOf" + id).innerHTML = quantity.toString();
            document.getElementById("totalPriceOf" + id).innerHTML = (singlePrice * quantity).toFixed(2);
            document.getElementById("cartPrice").innerHTML = (parseFloat(document.getElementById("cartPrice").innerHTML) + singlePrice).toFixed(2);
        }
    }else{

        let quantity = parseInt(document.getElementById("quantityOf" + id).value) + 1;
        let singlePrice = parseFloat(document.getElementById("singlePriceOf" + id).innerHTML);
        document.getElementById("quantityOf" + id).innerHTML = quantity.toString();
        document.getElementById("totalPriceOf" + id).innerHTML = (singlePrice * quantity).toFixed(2);
        document.getElementById("cartPrice").innerHTML = (parseFloat(document.getElementById("cartPrice").innerHTML) + singlePrice).toFixed(2);
    }

}

function subFromCart(id) {
    $.get("CartHandler?mode=sub&id=" + id,
        function() {
            let quantity = parseInt(document.getElementById("quantityOf" + id).innerHTML) - 1;
            let singlePrice = parseFloat(document.getElementById("singlePriceOf" + id).innerHTML);
            document.getElementById("cartPrice").innerHTML = (parseFloat(document.getElementById("cartPrice").innerHTML) - singlePrice).toFixed(2);
            if(quantity <= 0) {
                document.getElementById("item" + id).remove();
                if(document.getElementsByClassName("item").length === 0) {
                    document.getElementById("title").innerText = "Carrello vuoto";
                    document.getElementById("cartPrice").innerHTML = "0.00";
                }
            }
            else {
                document.getElementById("quantityOf" + id).innerHTML = quantity.toString();
                document.getElementById("totalPriceOf" + id).innerHTML = (singlePrice * quantity).toFixed(2);
            }
        }
    );
}

function remFromCart(id) {
    $.get("CartHandler?mode=remove&id=" + id,
        function() {
            let totalPriceOfEntry = parseFloat(document.getElementById("totalPriceOf" + id).innerHTML);
            document.getElementById("cartPrice").innerHTML = (parseFloat(document.getElementById("cartPrice").innerHTML) - totalPriceOfEntry).toFixed(2);
            document.getElementById("item" + id).remove();
            if(document.getElementsByClassName("item").length === 0) {
                document.getElementsByClassName("title").item(0) .innerText = "Carrello vuoto";
                document.getElementById("cartPrice").innerHTML = "0.00";
            }
        }
    );
}
