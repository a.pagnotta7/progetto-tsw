resizingSlide();
var max = 1;
var slideIndex = 1;
showDiResponsive(slideIndex);

function plusDivs(n) {
    showDiResponsive(slideIndex += n);
}

function currentDiv(n) {
    let windowWidth = window.innerWidth;
    if(windowWidth >= 980 && windowWidth <= 1452){
        max = 3;
        if(n > max)
            showDiResponsive(slideIndex = max);
        else
            showDiResponsive(slideIndex = n);
    }
    else if(windowWidth < 980){
        max = 6;
        if(n > max)
            showDiResponsive(slideIndex = max);
        else
            showDiResponsive(slideIndex = n);
    }else{
        max = 2;
        if(n > max)
            showDiResponsive(slideIndex = max);
        else
            showDiResponsive(slideIndex = n);
    }
}

function showDivs(n) {
    var i;
    var x = document.getElementsByClassName("card");
    var dots = document.getElementsByClassName("demo");
    if (n > x.length) {slideIndex = 1}
    if (n < 1) {slideIndex = x.length}
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" w3-red", "");
    }
    x[slideIndex-1].style.display = "block";
    dots[slideIndex-1].className += " w3-red";
}


function showDiv(){
    var i;
    var x = document.getElementsByClassName("card");
    var dot = document.getElementsByClassName("dot");


    if(slideIndex == 1) {
        x[0].style.display = "block";
        x[1].style.display = "block";
        x[2].style.display = "block";
        x[3].style.display = "none";
        x[4].style.display = "none";
        x[5].style.display = "none";
        dot[0].style.backgroundColor = "#717171";
        dot[1].style.backgroundColor = "#bbb";
        dot[2].display = "none";
        dot[3].display = "none";
        dot[4].display = "none";
        dot[5].display = "none";
    }
    if(slideIndex == 2) {
        x[0].style.display = "none";
        x[1].style.display = "none";
        x[2].style.display = "none";
        x[3].style.display = "block";
        x[4].style.display = "block";
        x[5].style.display = "block";
        dot[0].style.backgroundColor = "#bbb";
        dot[1].style.backgroundColor = "#717171";
        dot[2].display = "none";
        dot[3].display = "none";
        dot[4].display = "none";
        dot[5].display = "none";
    }
}

function dotResolver() {
    let windowWidth = window.innerWidth;
    window.console.log("-" + windowWidth);
    if(windowWidth >= 980 && windowWidth <= 1452){
        $("#dot1").show();
        $("#dot2").show();
        $("#dot3").show();
        $("#dot4").hide();
        $("#dot5").hide();
        $("#dot6").hide();
    }
    else if(windowWidth > 1452){
        $("#dot1").show();
        $("#dot2").show();
        $("#dot3").hide();
        $("#dot4").hide();
        $("#dot5").hide();
        $("#dot6").hide();
    }else{
        $("#dot1").show();
        $("#dot2").show();
        $("#dot3").show();
        $("#dot4").show();
        $("#dot5").show();
        $("#dot6").show();
    }
}


function resizingSlide() {
    window.setInterval("showDiResponsive()", 10);
    window.setInterval("dotResolver()", 10);
}

function showDiResponsive(){
    let windowWidth = window.innerWidth;
    window.console.log(windowWidth);
    var x = document.getElementsByClassName("card");
    var dot = document.getElementsByClassName("dot");

    if(windowWidth >= 980 && windowWidth <= 1452) {
        if(slideIndex > 3)
            slideIndex = 3;

        if (slideIndex == 1) {
            x[0].style.display = "block";
            x[1].style.display = "block";
            x[2].style.display = "none";
            x[3].style.display = "none";
            x[4].style.display = "none";
            x[5].style.display = "none";
            dot[0].style.backgroundColor = "#717171";
            dot[1].style.backgroundColor = "#bbb";
            dot[2].style.backgroundColor = "#bbb";
            dot[3].style.backgroundColor = "#bbb";
            dot[4].style.backgroundColor = "#bbb";
            dot[5].style.backgroundColor = "#bbb";
        }
        if (slideIndex == 2) {
            x[0].style.display = "none";
            x[1].style.display = "none";
            x[2].style.display = "block";
            x[3].style.display = "block";
            x[4].style.display = "none";
            x[5].style.display = "none";
            dot[0].style.backgroundColor = "#bbb";
            dot[1].style.backgroundColor = "#717171";
            dot[2].style.backgroundColor = "#bbb";
            dot[3].style.backgroundColor = "#bbb";
            dot[4].style.backgroundColor = "#bbb";
            dot[5].style.backgroundColor = "#bbb";
        }
        if (slideIndex == 3) {
            x[0].style.display = "none";
            x[1].style.display = "none";
            x[2].style.display = "none";
            x[3].style.display = "none";
            x[4].style.display = "block";
            x[5].style.display = "block";
            dot[0].style.backgroundColor = "#bbb";
            dot[1].style.backgroundColor = "#bbb";
            dot[2].style.backgroundColor = "#717171";
            dot[3].style.backgroundColor = "#bbb";
            dot[4].style.backgroundColor = "#bbb";
            dot[5].style.backgroundColor = "#bbb";
        }
    }
    else if(windowWidth < 980){
        if(slideIndex > 6)
            slideIndex = 6;

        if (slideIndex == 1) {
            x[0].style.display = "block";
            x[1].style.display = "none";
            x[2].style.display = "none";
            x[3].style.display = "none";
            x[4].style.display = "none";
            x[5].style.display = "none";
            dot[0].style.backgroundColor = "#717171";
            dot[1].style.backgroundColor = "#bbb";
            dot[2].style.backgroundColor = "#bbb";
            dot[3].style.backgroundColor = "#bbb";
            dot[4].style.backgroundColor = "#bbb";
            dot[5].style.backgroundColor = "#bbb";
        }
        if (slideIndex == 2) {
            x[0].style.display = "none";
            x[1].style.display = "block";
            x[2].style.display = "none";
            x[3].style.display = "none";
            x[4].style.display = "none";
            x[5].style.display = "none";
            dot[0].style.backgroundColor = "#bbb";
            dot[1].style.backgroundColor = "#717171";
            dot[2].style.backgroundColor = "#bbb";
            dot[3].style.backgroundColor = "#bbb";
            dot[4].style.backgroundColor = "#bbb";
            dot[5].style.backgroundColor = "#bbb";
        }
        if (slideIndex == 3) {
            x[0].style.display = "none";
            x[1].style.display = "none";
            x[2].style.display = "block";
            x[3].style.display = "none";
            x[4].style.display = "none";
            x[5].style.display = "none";
            dot[0].style.backgroundColor = "#bbb";
            dot[1].style.backgroundColor = "#bbb";
            dot[2].style.backgroundColor = "#717171";
            dot[3].style.backgroundColor = "#bbb";
            dot[4].style.backgroundColor = "#bbb";
            dot[5].style.backgroundColor = "#bbb";
        }
        if (slideIndex == 4) {
            x[0].style.display = "none";
            x[1].style.display = "none";
            x[2].style.display = "none";
            x[3].style.display = "block";
            x[4].style.display = "none";
            x[5].style.display = "none";
            dot[0].style.backgroundColor = "#bbb";
            dot[1].style.backgroundColor = "#bbb";
            dot[2].style.backgroundColor = "#bbb";
            dot[3].style.backgroundColor = "#717171";
            dot[4].style.backgroundColor = "#bbb";
            dot[5].style.backgroundColor = "#bbb";
        }
        if (slideIndex == 5) {
            x[0].style.display = "none";
            x[1].style.display = "none";
            x[2].style.display = "none";
            x[3].style.display = "none";
            x[4].style.display = "block";
            x[5].style.display = "none";
            dot[0].style.backgroundColor = "#bbb";
            dot[1].style.backgroundColor = "#bbb";
            dot[2].style.backgroundColor = "#bbb";
            dot[3].style.backgroundColor = "#bbb";
            dot[4].style.backgroundColor = "#717171";
            dot[5].style.backgroundColor = "#bbb";
        }
        if (slideIndex == 6) {
            x[0].style.display = "none";
            x[1].style.display = "none";
            x[2].style.display = "none";
            x[3].style.display = "none";
            x[4].style.display = "none";
            x[5].style.display = "block";
            dot[0].style.backgroundColor = "#bbb";
            dot[1].style.backgroundColor = "#bbb";
            dot[2].style.backgroundColor = "#bbb";
            dot[3].style.backgroundColor = "#bbb";
            dot[4].style.backgroundColor = "#bbb";
            dot[5].style.backgroundColor = "#717171";
        }
    }
    else{
        if(slideIndex > 2)
            slideIndex = 2;

        if(slideIndex == 1) {
            x[0].style.display = "block";
            x[1].style.display = "block";
            x[2].style.display = "block";
            x[3].style.display = "none";
            x[4].style.display = "none";
            x[5].style.display = "none";
            dot[0].style.backgroundColor = "#717171";
            dot[1].style.backgroundColor = "#bbb";
            dot[2].style.backgroundColor = "#bbb";
            dot[3].style.backgroundColor = "#bbb";
            dot[4].style.backgroundColor = "#bbb";
            dot[5].style.backgroundColor = "#bbb";
        }
        if(slideIndex == 2) {
            x[0].style.display = "none";
            x[1].style.display = "none";
            x[2].style.display = "none";
            x[3].style.display = "block";
            x[4].style.display = "block";
            x[5].style.display = "block";
            dot[0].style.backgroundColor = "#bbb";
            dot[1].style.backgroundColor = "#717171";
            dot[2].style.backgroundColor = "#bbb";
            dot[3].style.backgroundColor = "#bbb";
            dot[4].style.backgroundColor = "#bbb";
            dot[5].style.backgroundColor = "#bbb";
        }
    }
}